<?php
    session_start(); //session start

    // Include config file
    require_once "config.php";


?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>
    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Add New Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top"></P>
            
            <div class="uk-container-padded">
                <form class="uk-form uk-container-padded" name="book-pudo" action="admin-add-ride_submitted.php" method="POST">
                    
                    <fieldset class="uk-fieldset">
                        <div class="uk-width-1-1@m uk-margin">
                            <h3 class="uk-text-bold uk-text-purple">Personal Details</h3>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <label>First Name</label>
                                <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="" >
                            </div>
                            <div class="uk-width-1-2@m" >
                                <label>Last Name</label>
                                <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="" >
                            </div>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <label>Contact Number</label>
                            <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">We will update you through this number.</p>
                        </div>
                        <hr>
                        <div class="uk-width-1-1@m uk-margin-large-top">
                            <h3 class="uk-text-bold uk-text-purple">Ride Details</h3>
                        </div>

                        <div class="uk-margin uk-grid">
                            <div class="uk-width-1-2@m" >
                                <label>Category</label>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                    <label><input class="uk-checkbox" type="radio" name="category" value="Patient" <?=($category == "patient" ? "checked": "") ?> checked>  Patient</label>
                                    <label><input class="uk-checkbox" type="radio" name="category" value="Frontliner" <?=($category == "frontliner" ? "checked": "") ?> > Frontliner</label>
                                </div>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please select a category.</p>

                                <div class="uk-margin uk-grid-small uk-width-auto uk-margin-remove-bottom" hidden>
                                    <h3 class="uk-text-bold">Magandang Araw, Frontliner!</h3>
                                    <label><input class="uk-checkbox" type="radio" name="bike_status" value="Yes" <?=($bikeavailability == "yes" ? "checked": "") ?> checked> Yes, I can.</label>
                                    <label><input class="uk-checkbox" type="radio" name="bike_status" value="No" <?=($bikeavailability == "no" ? "checked": "") ?>> Nope</label>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <label>Passcode</label>
                                <input name="passcode" class="uk-input uk-text-bold" type="text" placeholder="" required>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted"> Passenger code: Name initials and last 4 digits of phone number.</p>
                            </div>
                        </div>
                        
                        <div class="uk-margin uk-width-auto">
                            <label>Pick-up Point</label>
                            <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <label>Drop-off Point</label>
                            <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                    
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <label>Pick-up Date</label>
                                <input name="date" class="uk-input" type="date" placeholder="" required>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <label>Pick-up Time</label>
                                <select name="time" class="uk-select" required>
                                    <option>06:00 AM</option>
                                    <option>06:30 AM</option>
                                    <option>07:00 AM</option>
                                    <option>07:30 AM</option>
                                    <option>08:00 AM</option>
                                    <option>08:30 AM</option>
                                    <option>09:00 AM</option>
                                    <option>09:30 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:30 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:30 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:30 PM</option>
                                    <option>01:00 PM</option>
                                    <option>01:30 PM</option>
                                    <option>02:00 PM</option>
                                    <option>02:30 PM</option>
                                    <option>03:00 PM</option>
                                    <option>03:30 PM</option>
                                    <option>04:00 PM</option>
                                    <option>04:30 PM</option>
                                    <option>05:00 PM</option>
                                    <option>05:30 PM</option>
                                    <option>06:00 PM</option>
                                    <option>06:30 PM</option>
                                    <option>07:00 PM</option>
                                    <option>07:30 PM</option>
                                    <option>08:00 PM</option>
                                    <option>08:30 PM</option>
                                    <option>09:00 PM</option>
                                    <option>09:30 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:30 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:30 PM</option>
                                    <option>12:00 AM</option>
                                    
                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Select Time</p>
                            </div>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <label>Amount willing to donate in the future</label>
                            <input name="budget" class="uk-input uk-text-bold" type="number" placeholder="0.00" >
                            <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">How much budget have you allocated for this trip.</p>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-width-1-1@m uk-margin-large-top">
                                <h3 class="uk-text-bold uk-text-purple">Additional Instructions</h3>
                                <p>Please enter any additioal instructions or special request in the box below.</p>
                            </div>
                            <div class="uk-width-1-1@m">
                                <textarea class="uk-textarea" name="message" rows="10" placeholder="Enter your message"></textarea>
                            </div>
                        </div>

                        <div class="uk-margin uk-grid">
                            <div class="uk-width-1-3@m" >
                                <label>Status</label>
                                <select name="status" class="uk-select uk-alert-warning" required>
                                    <option value="pending" <?=($status == "pending" ? "selected": "") ?> >Pending</option>
                                    <option value="received" <?=($status == "received" ? "selected": "") ?> >Received</option>
                                    <option value="scheduled" <?=($status == "scheduled" ? "selected": "") ?> >Scheduled</option>
                                    <option value="completed" <?=($status == "completed" ? "selected": "") ?> >Completed</option>
                                    <option value="bikerequest" <?=($status == "bikerequest" ? "selected": "") ?> hidden> Bike Request</option>
                                    <option value="bikelent" <?=($status == "bikelent" ? "selected": "") ?> hidden> Bike Lent</option>
                                    <option value="bikereturned" <?=($status == "bikereturned" ? "selected": "") ?> hidden> Bike Returned</option>
                                    <option value="Cancelled by Driver" <?=($status == "cancelleddriver" ? "selected": "") ?> >Cancelled by Driver</option>
                                    <option value="Cancelled by Rider" <?=($status == "cancelledrider" ? "selected": "") ?> >Cancelled by Rider</option>
                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Status</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <label>Assigned Driver</label>
                                <select class="uk-select" name="driver_id">
                                    <?=
                                        // Attempt select query execution
                                        $sql = "SELECT * from drivers";

                                        if($result = mysqli_query($link, $sql)){
                                            if(mysqli_num_rows($result) > 0){

                                                while($row = mysqli_fetch_assoc($result)){

                                                    echo '<option value="'.$row["id"].'" >'.$row["firstname"].' '.$row["lastname"].'</option>';
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        } 
                                    
                                    // Close connection
                                    mysqli_close($link);
                                    ?>
                                    
                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Assigned Driver</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <label>Trip Distance in kilometers</label>
                                <input name="distance" class="uk-input uk-text-bold uk-alert-success" type="number"  step="0.1" placeholder="0.0" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Trip Distance</p>
                            </div>
                        </div>
                        <div class="uk-margin-large-top uk-text-center">
                            <div class="uk-margin-bottom">
                                <input type="checkbox" class="uk-checkbox uk-text-bold" required> I confirm that the above information are correct and agree to the <a href="">Policies and Disclaimers</a>
                            </div>
                        </div>

                        <input class="uk-button uk-button-primary uk-width-1-1" type="submit" value="Confirm and Submit Booking" name="submit">
                    </fieldset>
                        
                    
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>