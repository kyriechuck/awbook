<?php
    session_start(); //session start
   
    // Include config file
    include "config.php";
    
    // Attempt select query execution
    $sql = "SELECT 
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' ORDER BY date ASC";

    $sql_week = "SELECT 
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) ORDER BY date ASC";

    $sql_sunday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 1 ORDER BY bookings.time ASC";

    $sql_monday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 2 ORDER BY bookings.time ASC";

    $sql_tuesday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 3 ORDER BY bookings.time ASC";

    $sql_wednesday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 4 ORDER BY bookings.time ASC";

    $sql_thursday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 5 ORDER BY bookings.time ASC";

    $sql_friday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 6 ORDER BY bookings.time ASC";

    $sql_saturday = "SELECT
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.status='scheduled' and YEARWEEK(bookings.date) = YEARWEEK(NOW()) and DAYOFWEEK(bookings.date) = 7 ORDER BY bookings.time ASC";
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">Scheduled Rides</h1>
                </div>
                <div class="uk-width-1-2@m ">
                    <a href="admin-add-ride.php" class="uk-button uk-button-primary uk-align-right">Add New Ride</a>
                </div>
            </div>
            
            
            <div class="uk-grid">
                <div class="uk-width-3-5@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li ><a href="admin-rides_pending.php">Pending Rides</a></li>
                        <li class="uk-active"><a href="admin-rides_scheduled.php">Scheduled Rides</a></li>
                        <li><a href="admin-rides_finished.php">Completed Rides</a></li>
                        <li><a href="admin-rides_cancelled.php">Cancelled Rides</a></li>
                        <li><a href="admin-rides_nad.php">NAD</a></li>
                    </ul>
                </div>
                <div class="uk-width-2-5@m ">
                    <div class="uk-grid">
                        <div class="uk-width-2-3@m">
                            <form action="admin-rides_searchresults.php" class="uk-search uk-search-default uk-align-right uk-flex-inline" method="POST">
                                <div class="">
                                    <span uk-search-icon></span>
                                    <input name="searchrides" class="uk-search-input uk-width-auto uk-text-small " type="search" placeholder="Search Rides">
                                </div>
                                <input class="uk-button uk-button-primary" type="submit" value="Go">
                            </form>
                        </div>
                        <div class="uk-width-1-3@m" hidden>
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="uk-margin-remove-bottom uk-text">These are rides which have assigned drivers and are now waiting for their set schedule.</p>
            <hr>

            
            <!-- main content -->
            <div class="uk-container-padded">
            <ul class="uk-subnav uk-subnav-pill" uk-switcher>
                <li class="uk-active"><a href="#">Scheduled This Week</a></li>
                <li><a href="#">All Scheduled</a></li>
            </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <p>Scheduled rides this week from Sunday to Saturday.</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead >
                                    <tr>
                                        <th class="uk-width-small">Date & Time</th>
                                        <th class="uk-width-small" >Passenger</th>
                                        <th class="uk-width-small">Origin</th>
                                        <th class="uk-width-small">Destination</th>
                                        
                                        <th class="uk-width-small">Driver</th>
                                        <th class="uk-width-small">Message</th>
                                        <th class="uk-width-small">Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered">
                            

                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <p class="uk-text-lead uk-text-bold">SUNDAY</p>
                                    <?php
                                        

                                        if($resultSunday = mysqli_query($link, $sql_sunday)){
                                            if(mysqli_num_rows($resultSunday) > 0){
                                                while($row = mysqli_fetch_array($resultSunday)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $pickup = $row["pickup"];
                                                    $phone = $row["phone"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . '<br>'.
                                                    $driverPhone . '</td> 
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultSunday);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>

                        </div>


                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">MONDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>
                                

                                <tbody>
                                    <?php
                                        

                                        if($resultMonday = mysqli_query($link, $sql_monday)){
                                            if(mysqli_num_rows($resultMonday) > 0){
                                                while($row = mysqli_fetch_array($resultMonday)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $pickup = $row["pickup"];
                                                    $phone = $row["phone"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . '<br>'.$driverPhone.'</td> 
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultMonday);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>


                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">TUESDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        

                                        if($resultTuesday = mysqli_query($link, $sql_tuesday)){
                                            if(mysqli_num_rows($resultTuesday) > 0){
                                                while($row = mysqli_fetch_array($resultTuesday)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $phone = $row["phone"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname .
                                                    '<br>'.$driverPhone. '</td> 
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultTuesday);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">WEDNESDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        

                                        if($resultWed = mysqli_query($link, $sql_wednesday)){
                                            if(mysqli_num_rows($resultWed) > 0){
                                                while($row = mysqli_fetch_array($resultWed)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $phone = $row["phone"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . 
                                                    '<br>'.$driverPhone.'</td>
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultWed);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">THURSDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        

                                        if($resultThur = mysqli_query($link, $sql_thursday)){
                                            if(mysqli_num_rows($resultThur) > 0){
                                                while($row = mysqli_fetch_array($resultThur)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $phone = $row["phone"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . '<br>'.$driverPhone. '</td>
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultThur);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">FRIDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        

                                        if($resultFri = mysqli_query($link, $sql_friday)){
                                            if(mysqli_num_rows($resultFri) > 0){
                                                while($row = mysqli_fetch_array($resultFri)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $phone = $row["phone"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . '<br>'.$driverPhone.'</td>
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultFri);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-overflow-auto uk-container-padded uk-container-bordered uk-margin-small-top">
                            <p class="uk-text-lead uk-text-bold">SATURDAY</p>
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead class="pm-0">
                                    <tr>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small" ></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                        <th class="uk-width-small"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        

                                        if($resultSat = mysqli_query($link, $sql_saturday)){
                                            if(mysqli_num_rows($resultSat) > 0){
                                                while($row = mysqli_fetch_array($resultSat)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $phone = $row["phone"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'.$time. '<br>' . $date .'</td>
                                                    <td class="uk-text-wrap">'. $name . '<br>'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $phone.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname .  '<br>'.$driverPhone.'</td>
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($resultSat);
                                            } else{
                                                echo "No bookings for this day.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    // mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </li>

                    <li>
                        <p>All scheduled rides.</p>
                        <div class="uk-overflow-auto">
                            <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                                <thead>
                                        <tr>
                                        <th class="uk-width-small" >Passenger</th>
                                        <th class="uk-width-small">Pick-up Point</th>
                                        <th class="uk-width-small">Drop-off Point</th>
                                        <th class="uk-width-small">PU Date and Time</th>
                                        <th class="uk-width-small">Driver</th>
                                        <th class="uk-width-small">Message</th>
                                        <th class="uk-width-small">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if($result = mysqli_query($link, $sql)){
                                            if(mysqli_num_rows($result) > 0){
                                                while($row = mysqli_fetch_array($result)){
                                                    $id = $row["id"];
                                                    $passcode = $row["passcode"];
                                                    $category = $row["category"];
                                                    $name = $row["firstname"] ." ". $row['lastname'];
                                                    $dropoff = $row["dropoff"];
                                                    $pickup = $row["pickup"];
                                                    $date_old = $row["date"];
                                                    $date = date("M d, Y", strtotime($date_old));
                                                    $time = $row["time"];
                                                    $status = $row["status"];
                                                    $message = $row["message"];
                                                    $budget = $row["budget"];
                                                    $driver_id = $row["driver_id"];
                                                    $driverFirstname = $row['driverFirstname'];
                                                    $driverLastname = $row['driverLastname'];
                                                    $driverPhone = $row['driverPhone'];
                                                    $distance = $row["distance"];
                                                    $date_added_old = $row["created_at"];
                                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                                    echo '<tr> 
                                                    <td class="uk-text-wrap">'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $category.'</td> 
                                                    <td class="uk-table-link" hidden>'. "<a class='uk-text-bold uk-text-green' href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$name</a>".'</td> 
                                                    <td class="uk-text-wrap">'.$pickup.'</td> 
                                                    <td class="uk-text-wrap">'.$dropoff. '</td> 
                                                    <td class="uk-text-wrap">'.$date. '<br>' . $time .'</td>
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$driverFirstname. ' '.$driverLastname . '<br>'. $driverPhone. '</td>
                                                    <td class="uk-text-wrap">'.$message. '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='admin-ride-view.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='admin-ride-update.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>" . "<a href='admin-ride_delete.php?id=". $row['id'] ."' title='Delete' data-toggle='tooltip'><span class='uk-icon' uk-icon='trash'></span></a>" .'</td>
                                                        </tr>';
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            } else{
                                                echo "No records matching your query were found.";
                                            }
                                        } else{
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                        }
                                    
                                    // Close connection
                                    mysqli_close($link);
                                    ?>
                                </tbody>
                            </table>
                        </div>


                    </li>

                </ul>
                
            </div>
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Completed Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>