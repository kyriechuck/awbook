<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>Driver - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_driver.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <?php

            if(!isset($_SESSION['loggedin']))//if session not found use default header
            {
                echo "<h1 class='uk-text-bold uk-text-purple uk-margin-remove-bottom'>Sorry, you are not authorized to access this section.</h1>";
            }else{  
                echo "<h1 class='uk-text-bold uk-text-purple uk-margin-remove-bottom'>Welcome, Driver</h1>";
            }
            ?>
            

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>