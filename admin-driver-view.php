<?php
session_start(); //session start

// Include config file
require_once "config.php";

// Check existence of id parameter before processing further
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    // Get URL parameter
    $id =  trim($_GET["id"]);
    
    // Prepare a select statement
    $sql = "SELECT * FROM drivers WHERE id = ?";
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);

            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $id = $row["id"];
                $firstname = $row["firstname"];
                $lastname = $row['lastname'];
                $name = $firstname .' '. $lastname;
                $phone = $row["phone"];
                $drivercode = $row["drivercode"];
                $address = $row["address"];
                $cartype = $row["cartype"];
                $carmodel = $row["carmodel"];
                $status = $row["status"];
                $facebook = $row['facebook'];
                $availability = $row['availability'];
                $created_atold = $row["created_at"];
                $created_at = date("M, d, Y", strtotime($created_atold));
            } else{
                // URL doesn't contain valid id. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
    
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
}  else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>


    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom"><?= $name ?></h1>
            <P class="uk-text uk-text uk-margin-remove-top">Driver Summary View</P>
            <p class="uk-text uk-text-bold uk-margin-remove-top">Driver Code: <?php echo $drivercode; ?> <br>Date Added: <?php echo $created_at; ?></p>
            
            <div class="uk-container-padded">
            <form class="uk-form uk-container-padded" name="driver-view" action="" method="POST">

                <fieldset class="uk-fieldset">
                    <h3 class="uk-text-bold">Personal Details</h3>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="firstname" class="uk-input uk-text-bold" type="text" value="<?php echo $firstname; ?>" placeholder="First Name" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">First name</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="lastname" class="uk-input uk-text-bold" type="text" value="<?php echo $lastname; ?>" placeholder="Last Name" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Last name</p>
                        </div>
                    </div>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                   
                            <input name="address" class="uk-input uk-text-bold" type="text" value="<?php echo $address; ?>" placeholder="Address" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Area of Coverage</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                   
                            <input name="drivercode" class="uk-input uk-text-bold" type="text" value="<?php echo $drivercode; ?>" placeholder="" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">6-digit Driver Code</p>
                        </div>

                    </div>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="firstname" class="uk-input uk-text-bold" type="text" value="<?php echo $facebook; ?>" placeholder="Facebook URL" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Facebook Profile URL</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="availability" class="uk-input uk-text-bold" type="text" value="<?php echo $availability; ?>" placeholder="Availability" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Availability</p>
                        </div>
                    </div>
                
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-3@m" >
                            <input name="phone" class="uk-input uk-text-bold" type="text" value="<?php echo $phone; ?>" placeholder="Contact Number" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Phone Number</p>
                        </div>
                        <div class="uk-width-1-3@m" >
                            <input name="car_type" class="uk-select uk-text-capitalize" value="<?php echo $cartype; ?>" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Car Type</p>
                        </div>
                        <div class="uk-width-1-3@m" >
                            <input name="car_model" class="uk-input uk-text-bold" type="text" value="<?php echo $carmodel; ?>" placeholder="Vehicle Model" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Vehicle model</p>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-width-1-3@m" >
                            <select name="status" class="uk-select uk-alert-warning" value="<?php echo $status; ?>" disabled>
                                <option value="active" <?=($status == "active" ? "selected": "") ?> >Active</option>
                                <option value="inactive" <?=($status == "inactive" ? "selected": "") ?> >Inactive</option>
                                <option value="warning" <?=($status == "warning" ? "selected": "") ?> >Warning</option>
                            </select>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Driver Status</p>
                        </div>
                    </div>
                    <?php echo "<a class='uk-button uk-button-primary uk-margin-top uk-width-1-1' href='admin-driver-update.php?id=".$row['id']."' title='Edit Driver Details' data-toggle='tooltip'>Edit Details <span class='uk-icon' uk-icon='file-edit'></span></a>"; ?>
                </fieldset>
                    
                
            </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>