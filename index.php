<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>AccessiWheels - Driving people towards equal opportunities</title>
        <meta property=”og:title” content=”AccessiWheels - Driving people towards equal opportunities” />
        <meta property="og:description" content="AccessiWheels is a social enterprise which enables people with mobility problems to be where they need to be. We connect customers to trained drivers to ensure that they travel safely and conveniently.">
        <meta property=”og:image content=”http://lilypaddigital.co/mediabox/images/UI/accessiwheels-bookingplatform.jpg” />
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['Username']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>




    <!-- start main section -->
    <div class="uk-section uk-section-lightbackground">
		<div class="uk-container uk-container-small">
            
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Welcome to Accessiwheels</h1>
            <h3 class="uk-text-bold uk-text-green uk-margin-remove-top">Driving people towards equal opportunities.</h3>
            
            <a href="book.php" class="uk-button uk-button-primary uk-margin">Book A Ride</a>
            <a href="covid19.php" class="uk-button uk-button-secondary">Read: Our Covid Initiative</a>

        </div>
    </div>

    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <p class="uk-text-center uk-text-large">We are <span class="uk-text-bold uk-text-green">AccessiWheels</span>. We enable all users to be where they <span class="uk-text-bold uk-text-purple">WANT</span> to be. <br>We connect customers to well-trained drivers to ensure <span class="uk-text-bold uk-text-purple">assured, convenient,</span> and <span class="uk-text-bold uk-text-purple">safe</span> travels.</p>
    </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>