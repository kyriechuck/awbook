<?php
    session_start(); //session start

    // Include config file
    include "config.php";

?>

<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        
        <title>Ride History - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    include "includes/nav_user.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Ride History</h1>
            <P class="uk-text uk-text-lead uk-margin-remove-top">Browse all your rides with AccessiWheels</P>
            
            <!-- main content -->
            <div class="uk-container-padded">
                <div class="uk-overflow-auto">
                    
                    <?php
                        $query = $_POST['searchhistory']; 
                        // gets value sent over search form
                        // Attempt select query execution
                        $sql = "SELECT * FROM bookings where passcode like '%".$query."%' ORDER BY created_at DESC";
                        if($result = mysqli_query($link, $sql)){
                            if(mysqli_num_rows($result) > 0){
                                
                                echo "<table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>";
                                    echo '<thead>
                                             <tr>
                                                <th class="uk-width-small">Pick-up Point</th>
                                                <th class="uk-width-small">Drop-off Point</th>
                                                <th class="uk-width-small">PU Date and Time</th>
                                                <th class="uk-width-small">Date Added</th>
                                                <th class="uk-table-shrink">Status</th>
                                            </tr>
                                        </thead>';
                                while($row = mysqli_fetch_array($result)){
                                    $id = $row['id'];
                                    $passcode = $row["passcode"];
                                    $category = $row["category"];
                                    $name = $row["firstname"] ." ". $row['lastname'];
                                    $pickup = $row["pickup"];
                                    $dropoff = $row["dropoff"];
                                    $date_old = $row["date"];
                                    $date = date("M, d, Y", strtotime($date_old));
                                    $time = $row["time"];
                                    $status = $row["status"];
                                    $message = $row["message"];
                                    $budget = $row["budget"];
                                    $date_added_old = $row["created_at"];
                                    $date_added = date("M, d, Y", strtotime($date_added_old));

                                    echo '<tr> 
                                             <td class="uk-text-wrap">'.$pickup.'</td> 
                                            <td class="uk-text-wrap">'.$dropoff.'</td>
                                            <td class="uk-text-wrap">'.$date. '<br>' . $time .'</td>
                                            <td class="uk-text-wrap">'.$date_added.'</td>  
                                            <td class="uk-text-wrap uk-text-capitalize">' .$status . '</td>';
                                }
                                echo "</table>";
                                // Free result set
                                mysqli_free_result($result);
                            } else{
                                echo "No records matching your query were found.";
                            }
                        } else{
                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                        }
                    
                    // Close connection
                    mysqli_close($link);
                    ?>

                </div>
            </div>
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">My Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>