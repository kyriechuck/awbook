<?php include('process-booking.php'); ?>
<?php
    session_start(); //session start
    
    if ( !session_id() ) {
        session_start();
    }
    $firstname = $_SESSION['firstname'];
    $lastname = $_SESSION['lastname'];
    $phone = $_SESSION['phone'];
?>
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>Book a Ride - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

<?php 
// for demo and simulation of passenger code generation
$fname = substr($firstname,0,1);
$lname = substr($lastname,0,1);
$pnumber = substr($phone,-4);

$passenger_code = $fname . $lname . $pnumber;
 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-green uk-margin-remove-bottom">Booking request submitted!</h1>
            <h2 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Thank you <?php echo $firstname; ?> <?php echo $lastname;; ?></h2>
            <P class="uk-text uk-margin-remove-top">We have received your booking request. A confirmation will be sent within the next 12 hours.
        </P>

             <!-- main content -->
            <div class="uk-container-small uk-container-padded">
                <p class="">Here is your unique passenger code to track your ride status.</p>
                <h3 class="uk-text-bold uk-margin-remove-top">Passenger Code: <?php echo $passenger_code; ?> </h3>
            </div>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>