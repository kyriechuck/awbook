
<?php
    session_start(); //session start

    //store posted values in session variables
    $_SESSION['username'] = $_POST['username'];
    $_SESSION['password'] = $_POST['password'];
    
?> 


<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>
 
<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Sign Up</h1>
            <P class="uk-text uk-text uk-margin-remove-top">A summary of your sign up is shown below. To make changes, click on the Edit Details button.</P>
            
            <div class="uk-container-padded">
                <form class=" uk-form " name="book-confirm" method="POST" action="signup-submitted.php">
                    <div class="uk-grid">
                        <div class="uk-width-1-1 form-title">
                            <h3 class="uk-text-bold uk-text-purple">Account Details</h3>
                        </div>
                        <div class="uk-width-1-1 form-step">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Username</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["username"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Password</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["password"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Email Address</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["email"]; ?> </h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="uk-width-1-1 form-title uk-margin-large-top">
                            <h3 class="uk-text-bold uk-text-purple">Personal Details</h3>
                        </div>
                        <div class="uk-width-1-1 form-step">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Name</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["firstname"]; ?> <?php echo $_POST["lastname"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Address</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["address"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Contact Number</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["phone"]; ?> </h4>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="uk-margin uk-text-center uk-margin-large-top">
                        <a href="book-pudo.php" class="uk-button">Edit Details</a>
                        <input class="uk-button uk-button-primary" type="submit" value="Confirm and Submit Booking" name="submit-confirmed">
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>