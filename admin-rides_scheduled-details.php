<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <h1 class="uk-text-bold uk-text-purple">Rides</h1>
                </div>
                <div class="uk-width-1-3@m">
                    <button class="uk-button uk-button-primary uk-align-right">Add New Ride</button>
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li ><a href="admin-rides_pending.php">Pending Rides</a></li>
                        <li class="uk-active"><a href="admin-rides_scheduled.php">Scheduled Rides</a></li>
                        <li><a href="admin-rides_finished.php">Finished Rides</a></li>
                        <li><a href="admin-rides_cancelled.php">Cancelled Rides</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-3@m ">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <form class="uk-search uk-search-default uk-align-right">
                                <span uk-search-icon></span>
                                <input class="uk-search-input uk-text-small " type="search" placeholder="Search Rides">
                            </form>
                        </div>
                        <div class="uk-width-1-2@m">
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p>These are rides which have assigned drivers and are now waiting for their set schedule.</p>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                <a href="#"><p class="uk-text-small">Go Back</p></a>

                <div class="uk-grid ">
                    <div class="uk-width-3-5@m">
                        <h3 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Nadine Lustre</h3>
                        <p class="uk-text uk-margin-remove-top">Passenger Code: JD9332</p>
                        <p class="uk-text uk-margin-remove-top">Date Added: April 10, 2020</p>
                    </div>
                    <div class="uk-width-1-5@m uk-text-right@m">
                        <p class="uk-margin-remove-bottom uk-text-small uk-text-bold">ASSIGNED DRIVER</p>
                        <div class="uk-margin">
                            <select class="uk-select">
                                <option>Melvin Dela Cruz</option>
                                <option>Oscar Reyes</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-5@m uk-text-right@m">
                        <p class="uk-margin-remove-bottom uk-text-small uk-text-bold">STATUS</p>
                        <div class="uk-margin">
                            <select class="uk-select">
                                <option>Driver Assigned</option>
                                <option>Pending</option>
                                <option>Received</option>
                                <option>Completed</option>
                                <option>Cancelled</option>
                                <option>Driver Cancelled</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                
                <hr>
                
                <div class="uk-margin uk-width-auto">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP POINT</p>
                            <h4 class="uk-margin-small uk-text-purple">Maalalahanin, Teachers Village, Quezon City</h4>
                        </div>
                        <div class="uk-width-1-2@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">DROP-OFF POINT</p>
                            <h4 class="uk-margin-small uk-text-purple">East Avenue Medical Center, Quezon City</h4>
                        </div>
                        
                    </div>
                    <hr>
                    <div class="uk-grid ">
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP TIME</p>
                            <h4 class="uk-margin-small ">10:30 AM</h4>
                        </div>
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP DATE</p>
                            <h4 class="uk-margin-small">Apr 10, 2020</h4>
                        </div>
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text">CAR TYPE</p>
                            <h4 class="uk-margin-small ">Sedan</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="uk-grid">
                        <div class="uk-width-2-3@m">
                            <p class=" uk-margin uk-text-small uk-text-bold">MESSAGE TO DRIVER</p>
                            <p class="uk-margin-small">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">BUDGET</p>
                            <div class="uk-margin">
                                <input class="uk-input uk-width-small uk-text-bold" type="text" placeholder="N/A" >
                            </div>
                        </div>
                    </div>

                    <div class="uk-margin">
                        <hr>
                        <a class="uk-button uk-button-danger uk-padding-small uk-text-small">Back to List</a>
                        <a class="uk-button uk-button-primary uk-padding-small uk-text-small">Save Changes</a>
                    </div>
                </div>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>