<?php
    session_start(); //session start
    
    // Include config file
    require_once "config.php";
    
    // Attempt select query execution
    $sql = "SELECT 
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.id = ? ";
?>
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>AccessiWheels Pending Ride Requests</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

<?php
// Check existence of id parameter before processing further
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = trim($_GET["id"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
    
            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $id = $row["id"];
                $category = $row["category"];
                $passcode = $row["passcode"];
                $name = $row["firstname"] . " " . $row["lastname"];
                $firstname = $row["firstname"];
                $lastname = $row["lastname"];
                $phone = $row["phone"];
                $pickup = $row["pickup"];
                $dropoff = $row["dropoff"];
                $date = $row["date"];
                $date_pickup = date("M, d, Y", strtotime($date));
                $time = $row["time"];
                $message = $row["message"];
                $budget = $row["budget"];
                $driver_id = $row["driver_id"];
                $driverFirstname = $row['driverFirstname'];
                $driverLastname = $row['driverLastname'];
                $status = $row["status"];
                $distance = $row["distance"];
                $date_addedold = $row["created_at"];
                $date_added = date("M, d, Y", strtotime($date_addedold));

                $driverFirstname = $row['driverFirstname'];
                $driverLastname = $row['driverLastname'];
            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                <a href="pending-rides.php"><p class="uk-text-small">Go Back</p></a>
                <h2 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Booking request summary view</h2>
                <p class="uk-text uk-text-bold uk-margin-remove-top">Date Requested: <?php echo $date_added; ?></p>
                
                <form class="uk-form uk-container-padded"  action="admin-ride-update.php" method="POST">
                    
                    <fieldset class="uk-fieldset">
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <h3 class="uk-text-bold uk-margin-remove-bottom uk-text-purple"><?php echo $passcode; ?></h3>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Passenger Code</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <h4 class="uk-text-bold uk-margin-remove-bottom"><?php echo $category; ?></h4>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Category</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <h4 class="uk-text-bold uk-margin-remove-bottom"><?php echo $date_pickup; ?> / <?php echo $time; ?></h4>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Pick-up Date & Time</p>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m">
                                <p class="uk-margin-remove-bottom uk-text-large uk-text-bold"><?php echo $pickup; ?></p>
                                <p class="uk-text uk-margin-remove-top uk-text-muted"><span class="uk-text-bold">Pick-up Point:</span> Please provide complete street address, hotel / hospital / airport name, and city.</p>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class="uk-margin-remove-bottom uk-text-large uk-text-bold"><?php echo $dropoff; ?></p>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted"><span class="uk-text-bold">Drop-off Point: </span>Please provide complete street address, hotel / hospital / airport name, and city.</p>
                            </div>
                        </div>
                        <hr>
                    
                        
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-2-3@m" >
                                <p class="uk-text-large"><?php echo $message; ?></p>
                                <p class="uk-text uk-margin-remove-top uk-text-muted">Additional message or instructions</p>
                            </div>
                        </div>
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>