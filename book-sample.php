<?php include ('process-booking.php'); ?>
<?php
    session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>
    <!-- start main section -->
    <div class="uk-section uk-section-lightbackground">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">All fields are required.</P>
            
            <div class="uk-grid ">    
                <div class="uk-width-2-3@m" >
                    <div class="uk-container ">
                        <form class="uk-form uk-container-padded" name="book-pudo" action="book-sample.php" method="POST">
                            <fieldset class="uk-fieldset">
                                <div class="uk-grid uk-margin">
                                    <div class="uk-width-1-2@m" >
                                        <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name"  value="<?php echo $firstname; ?>" required>
                                    </div>
                                    <div class="uk-width-1-2@m" >
                                        <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $lastname; ?>" required>
                                    </div>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number">
                                    <p class="uk-text-small uk-margin-remove-top uk-text-muted">We will update you through this number.</p>
                                </div>
                                <hr>

                                <input class="uk-button uk-button-primary uk-width-1-1" type="submit" value="Review Booking" name="submit">
                            </fieldset>
                                
                            
                        </form>
                    </div>
                </div>

                <div class="uk-width-1-3@m">
                </div>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>