<?php 
    session_start(); //session start


    // Include config file
    include "config.php";
  
    // Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    $passcode = trim($_POST["passcode"]);
    $category = trim($_POST["category"]);
    $bike_status = trim($_POST["bike_status"]);
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $phone = trim($_POST["phone"]);
    $pickup = mysqli_real_escape_string($link, $_POST['pickup']);
    $dropoff = mysqli_real_escape_string($link, $_POST['dropoff']);
    $date = trim($_POST["date"]);
    $time = trim($_POST["time"]);
    $budget = trim($_POST["budget"]);
    $message = mysqli_real_escape_string($link, $_POST['message']);
    $status = trim($_POST["status"]);
    $driver_id = trim($_POST["driver_id"]);
    $distance = trim($_POST["distance"]);

    // Prepare an update statement
    $sql = "UPDATE bookings SET passcode='$passcode', category='$category', bike_status='$bike_status', firstname='$firstname', lastname='$lastname', phone='$phone', pickup='$pickup', dropoff='$dropoff', date='$date', time='$time', budget='$budget', message='$message', status='$status', driver_id=$driver_id, distance='$distance' WHERE id=$id ";
        
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "sssi", $param_passcode, $param_category, $param_bikestatus, $param_firstname, $param_lastname,  $param_phone, $param_pickup, $param_dropoff, $param_date,  $param_time, $param_budget, $param_message, $param_status, $param_driverid, $param_distance, $param_id);
        
        // Set parameters
        $param_passcode = $passcode;
        $param_category = $category;
        $param_bikestatus = $bike_status;
        $param_firstname = $firstname;
        $param_lastname = $lastname;
        $param_phone = $phone;
        $param_pickup = $pickup;
        $param_dropoff = $dropoff;
        $param_date = $date;
        $param_time = $time;
        $param_budget = $budget;
        $param_message = $message;
        $param_status = $status;
        $param_driverid = $driver_id;
        $param_distance = $distance;
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records updated successfully. Redirect to landing page
            $_SESSION['success'] = " <div class='uk-padding uk-alert-success uk-text-large uk-text-center' uk-alert>
                                <a class='uk-alert-close' uk-close></a>
                                <p>You have successfully updated the ride request details for <span class='uk-text-bold'>$firstname $lastname</span> !</p>
                            </div> ";
        } else{
            echo "Something went wrong. Please try again later.";
        }
    }
        
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
    

}
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>
<?php 
// for demo and simulation of passenger code generation
$fname = substr($firstname,0,1);
$lname = substr($lastname,0,1);
$pnumber = substr($phone,-4);

$passenger_code = $fname . $lname . $pnumber;
 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">

             <!-- main content -->
            <div class="uk-container-small uk-container-padded">
                <h3><?php echo $_SESSION['success']; ?></h3>
                <div class="uk-padding-small uk-text-center">
                    <a href="admin-rides_pending.php" class="uk-button uk-button-default">View Rides</a>
                    <a href="admin-add-ride.php" class="uk-button uk-button-primary">Add New Ride</a>
                </div>
                <p class="uk-text-center uk-margin-remove-bottom">Below is the generated passenger code for <?php echo $firstname; ?> <?php echo $lastname;; ?></p>
                <h3 class="uk-text-bold uk-margin-remove-top uk-text-center">Passenger Code: <?php echo $passenger_code; ?> </h3>
            </div>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>