<?php
    session_start(); //session start

// Include config file
require_once "config.php";
 

 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $phone = trim($_POST["phone"]);
    $address = trim($_POST["address"]);
    $cartype = trim($_POST["cartype"]);
    $carmodel = trim($_POST["carmodel"]);
    $status = trim($_POST["status"]);
    $facebook = trim($_POST["facebook"]);
    $availability = trim($_POST["availability"]);
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) ){
        
        // Prepare an insert statement ('$username', '$password', '$firstname', '$lastname', $email, $phone, $address, $carmodel)
        $sql = "INSERT 
        INTO drivers (firstname, lastname, phone, address, cartype, carmodel, status, facebook, availability) 
        VALUES ( '$firstname', '$lastname', '$phone', '$address', '$cartype', '$carmodel', '$status', '$facebook', '$availability')";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password, $param_firstname, $param_lastname, $param_email, $param_phone, $param_address, $param_cartype, $param_carmodel, $param_status, $param_facebook, $param_availability);
            
            // Set parameters
            $param_firstname = $firstname;
            $param_lastname = $lastname;
            $param_phone = $phone;
            $param_address = $addres;
            $param_cartype = $cartype;
            $param_carmodel = $carmodel;
            $param_status = $status;
            $param_facebook = $facebook;
            $param_availability = $availability;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                $_SESSION['success'] = " <div class='uk-padding uk-alert-success uk-text-large uk-text-center' uk-alert>
                                <a class='uk-alert-close' uk-close></a>
                                <p>You have successfully added <span class='uk-text-bold'>$firstname $lastname</span> as one of AW partner driver!</p>
                            </div> ";
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    
    // Close connection
    mysqli_close($link);
}

?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>
<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>


    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <!-- notification message -->
            <?php if (isset($_POST['submit'])) : ?>
                <div class="error success" >
                    <h3>
                        <?php 
                            echo $_SESSION['success']; 
                            unset($_SESSION['success']);
                        ?>
                    </h3>
                </div>
            <?php endif ?>
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Add New Driver</h1>
            <P class="uk-text uk-text uk-margin-remove-top">All fields are required.</P>
            
            <div class="uk-container-padded">
            <form class="uk-form uk-container-padded" name="book-pudo" action="admin-add-driver.php" method="POST">
                <fieldset class="uk-fieldset">
                    <h3 class="uk-text-bold">Personal Details</h3>
                    <p>Please fill out all fields.</p>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                        </div>
                    </div>
                    <div class="uk-margin uk-width-auto">
                        <input name="address" class="uk-input uk-text-bold" type="text" placeholder="Coverage" >
                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Area of coverage.</p>
                    </div>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="facebook" class="uk-input uk-text-bold" type="text" placeholder="Facebook URL" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter facebook profile url</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="availability" class="uk-input uk-text-bold" type="text" placeholder="Driver availability" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter availability</p>
                        </div>
                    </div>
                
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-3@m" >
                            <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter contact number</p>
                        </div>
                        <div class="uk-width-1-3@m" >
                            <select name="cartype" class="uk-select" >
                                <option value="sedan" <?=($cartype == "sedan" ? "selected": "") ?> >Sedan</option>
                                <option value="van" <?=($cartype == "van" ? "selected": "") ?> >Van</option>
                                <option value="auv" <?=($cartype == "auv" ? "selected": "") ?> >AUV</option>
                                <option value="ambulance" <?=($cartype == "ambulance" ? "selected": "") ?> >Ambulance</option>
                                <option value="uv express" <?=($cartype == "uv express" ? "selected": "") ?> >UV Express</option>
                            </select>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Car Type</p>
                        </div>
                        <div class="uk-width-1-3@m" >
                            <input name="carmodel" class="uk-input uk-text-bold" type="text" placeholder="Vehicle Model" value="N/A">
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter vehicle model</p>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <div class="uk-width-1-3@m" >
                            <select name="status" class="uk-select" >
                                <option value="active" <?=($status == "active" ? "selected": "") ?> >Active</option>
                                <option value="inactive" <?=($status == "inactive" ? "selected": "") ?> >Inactive</option>
                                <option value="warning" <?=($status == "warning" ? "selected": "") ?> >Warning</option>
                            </select>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Driver Status</p>
                        </div>
                    </div>
                    <input class="uk-button uk-button-primary uk-width-1-1 uk-margin-top" type="submit" value="Submit" name="submit">
                </fieldset>
                    
                
            </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>