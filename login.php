<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: index-rider.php");
    exit;
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Redirect user to welcome page
                            header("location: index-rider.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>


    <!-- start main section -->
    <div class="uk-section uk-section-fullheight uk-section-lightbackground">
        <div class="uk-container uk-container-small">
            
            <div class="uk-grid">
                <div class="uk-width-1-2@s uk-width-2-3@m uk-width-1-2@l" >
                    <div class="uk-container ">
                        <h1 class="uk-text-bold uk-text-green uk-margin">Log in to AccessiWheels</h1>

                        <form class="uk-form uk-form-bordered uk-container-padded" action="login.php" method="post">
                            <fieldset class="uk-fieldset">
                                    <div class="uk-margin">
                                        <div class="uk-inline uk-width-1-1@s">
                                            <span class="uk-form-icon" uk-icon="icon: user"></span>
                                            <input name="username" class="uk-input" type="text" placeholder="Email or Username">
                                        </div>
                                    </div>
                                    <div class="uk-margin">
                                    <div class="uk-inline uk-width-1-1@s">
                                            <span class="uk-form-icon" uk-icon="icon: lock"></span>
                                            <input name="password" class="uk-input" type="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="uk-margin">
                                        <input type="checkbox">
                                        <span class="uk-text-small">Remember Me</span>
                                    </div>
                                    <input type="submit" name="login-submit" class="uk-button uk-button-primary uk-width-1-1" value="Login" disabled="">
                                    <div class="uk-margin">
                                        <p class="uk-text-small">Don't have an account?  <a class="uk-text-small" href="#">Sign up now.</a></p> 
                                    </div>
                            </fieldset>
                        </form>
                        <p class=" uk-text-center"><img src="https://lilypaddigital.co/mediabox/images/UI/powered-by-lilypad.png" title="Powered by Lily Pad Digital Solutions" width="220" alt="Powered by Lily Pad Digital Solutions"/></p>
                    </div>
                </div>

                <div class="uk-width-1-2@s uk-width-1-3@m uk-width-1-2@l">
                </div>
            </div>
        </div>
    </div>
    <!-- end main section -->

</body>
</html>