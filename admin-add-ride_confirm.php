<?php
    session_start(); //session start
        // Include config file
    include "config.php";

    //store posted values in session variables
    $_SESSION['category'] = $_POST['category'];
    $_SESSION['passcode'] = $_POST['passcode'];
    $_SESSION['bike_status'] = $_POST['bike_status'];
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['phone'] = $_POST['phone'];
    $_SESSION['pickup'] = $_POST['pickup'];
    $_SESSION['dropoff'] = $_POST['dropoff'];
    $_SESSION['date'] = $_POST['date'];
    $_SESSION['time'] = $_POST['time'];
    $_SESSION['budget'] = $_POST['budget'];
    $_SESSION['status'] = $_POST['status'];
    $_SESSION['message'] = $_POST['message'];
    $_SESSION['driver_id'] = $_POST['driver_id'];
    $_SESSION['distance'] = $_POST['distance'];
    
?> 

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">A summary of your booking is shown below. To make changes, click on the Edit Details button.</P>
            
            <div class="uk-container-padded">
                <form class=" uk-form " name="book-confirm" method="POST" action="admin-add-ride_submitted.php">
                    <div class="uk-grid">
                        <div class="uk-width-1-1@">
                            <h3 class="uk-text-bold uk-text-purple">Your Details</h3>
                        </div>
                        <div class="uk-width-1-1@m">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Name</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["firstname"]; ?> <?php echo $_POST["lastname"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Contact Number</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["phone"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Passenger Code</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["passcode"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Category</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["category"]; ?> </h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="uk-width-1-1 form-title uk-margin-large-top">
                            <h3 class="uk-text-bold uk-text-purple">Ride Details</h3>
                        </div>
                        <div class="uk-width-1-1 form-step">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Pick-up Point</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["pickup"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Drop-off Point</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["dropoff"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Pick-up Time</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["time"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Pick-up Date</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["date"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Budget</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["budget"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Additional Message</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["message"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Assigned Driver</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["driver_id"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Distance Travelled</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["distance"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Status</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["status"]; ?> </h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="uk-margin uk-text-center">
                        <div class="uk-margin-bottom">
                            <input type="checkbox" required> I confirm that the above information are correct and agree to the <a href="">Policies and Disclaimers</a>
                        </div>

                        <a href="book-pudo.php" class="uk-button">Edit Details</a>
                        <input class="uk-button uk-button-primary" type="submit" value="Confirm and Submit Booking" name="submitconfirmed">
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>