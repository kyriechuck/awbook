<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        
        <title>Ride Status - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Ride Status</h1>
            <P class="uk-text uk-text-lead uk-margin-remove-top">Check the status of your ride.</P>

            <!-- main content -->
            <div class="uk-container-small uk-container-padded">
                <form class="uk-grid-small" action="ride-status_detail.php" method="POST" uk-grid>
                    <div class="uk-width-4-5@m">
                        <input name="query" class="uk-padding-medium uk-input" placeholder="Example: CB8972" required>
                        <p class="uk-text-bold uk-margin-small-top">Enter your 6-digit alphanumeric <span class="uk-text-green">Passenger Code</span></p>
                    </div>
                    <div class="uk-width-1-5@m">
                        <input class="uk-button uk-button-primary" type="submit" value="Check Status">
                    </div>
                </form>
            </div>
            <!-- end main content -->
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>