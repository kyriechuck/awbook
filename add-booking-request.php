<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$firstname = $lastname = $phone = $pickup = $dropoff = $date = $time = $status = $budget = "";
$firstname_err = $lastname_err = $phone_err = $pickup_err = $dropoff_err = $date_err = $time_err = $status_err = $budget_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate first name
    $input_firstname = trim($_POST["firstname"]);
    if(empty($input_name)){
        $firstname_err = "Please enter your first name.";
    } elseif(!filter_var($input_firstname, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $firstname_err = "Please enter a valid name.";
    } else{
        $firstname = $input_firstname;
    }

    // Validate last name
    $input_lastname = trim($_POST["lastname"]);
    if(empty($input_name)){
        $lastname_err = "Please enter your last name.";
    } elseif(!filter_var($input_lastname, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $lastname_err = "Please enter a valid name.";
    } else{
        $lastname = $input_lastname;
    }
    
    // Validate phone
    $input_phone = trim($_POST["phone"]);
    if(empty($input_phone)){
        $phone_err = "Please enter phone number.";     
    } else{
        $phone = $input_phone;
    }

    // Validate pickup
    $input_pickup = trim($_POST["pickup"]);
    if(empty($input_phone)){
        $pickup_err = "Please enter pick-up point.";     
    } else{
        $pickup = $input_pickup;
    }
    
    // Check input errors before inserting in database
    if(empty($firstname_err) && empty($lastname_err) && empty($phone_err) && empty($pickup_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO bookings (firstname, lastname, phone, pickup) VALUES (?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_firstname, $param_lastname, $param_phone, $param_pickup);
            
            // Set parameters
            $param_firstname = $firstname;
            $param_lastname = $lastname;
            $param_phone = $phone;
            $param_pickup = $pickup;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: booking-submitted.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>



<?php
    session_start(); //session start
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>


    <!-- start main section -->
    <div class="uk-section uk-section-lightbackground">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">All fields are required.</P>
            
            <div class="uk-grid ">    
                <div class="uk-width-2-3@m" >
                    <div class="uk-container ">
                        <form class="uk-form uk-container-padded" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            
                            <fieldset class="uk-fieldset">
                                <div class="uk-grid uk-margin">
                                    <div class="uk-width-1-2@m" >
                                        <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name"  value="<?php echo $firstname; ?>" required>
                                    </div>
                                    <div class="uk-width-1-2@m" >
                                        <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $lastname; ?>" required>
                                    </div>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" value="<?php echo $phone; ?>">
                                    <p class="uk-text-small uk-margin-remove-top uk-text-muted">We will update you through this number.</p>
                                </div>
                                <hr>
                                <div class="uk-margin uk-width-auto">
                                    <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="Pick-up Point" value="<?php echo $pickup; ?>">
                                    <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="Drop-off Point" >
                                    <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                                </div>
                            
                                <div class="uk-grid uk-margin">
                                    <div class="uk-width-1-2@m" >
                                        <input name="date" class="uk-input" type="date" placeholder="" >
                                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                                    </div>
                                    <div class="uk-width-1-2@m" >
                                        <select name="time" class="uk-select" >
                                            <option>07:00 AM</option>
                                            <option>07:30 AM</option>
                                            <option>08:00 AM</option>
                                            <option>08:30 AM</option>
                                            <option>09:00 AM</option>
                                            <option>09:30 AM</option>
                                            <option>10:00 AM</option>
                                            <option>10:30 AM</option>
                                            <option>11:00 AM</option>
                                            <option>11:30 AM</option>
                                            <option>12:00 PM</option>
                                            <option>12:30 PM</option>
                                            <option>01:00 PM</option>
                                            <option>01:30 PM</option>
                                            <option>02:00 PM</option>
                                            <option>02:30 PM</option>
                                            <option>03:00 PM</option>
                                            <option>03:30 PM</option>
                                            <option>04:00 PM</option>
                                            <option>04:30 PM</option>
                                            <option>05:00 PM</option>
                                            <option>05:30 PM</option>
                                            <option>06:00 PM</option>
                                            <option>06:30 PM</option>
                                            <option>07:00 PM</option>
                                            <option>07:30 PM</option>
                                            <option>08:00 PM</option>
                                            <option>08:30 PM</option>
                                            <option>09:00 PM</option>
                                            <option>09:30 PM</option>
                                            <option>10:00 PM</option>
                                            <option>10:30 PM</option>
                                            <option>11:00 PM</option>
                                            <option>11:30 PM</option>
                                            <option>12:00 AM</option>
                                            
                                        </select>
                                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Time</p>
                                    </div>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <input name="budget" class="uk-input uk-text-bold" type="number" placeholder="Budget" >
                                    <p class="uk-text-small uk-margin-remove-top uk-text-muted">How much budget have you allocated for this trip.</p>
                                </div>
                                <div class="uk-margin uk-width-auto" hidden>
                                    <select name="status" class="uk-select" value="pending">
                                        <option value="pending">Pending</option>
                                        <option value="received">Received</option>
                                        <option value="assigned-driver">Assigned Driver</option>
                                        <option value="scheduled">Scheduled</option>
                                </select>
                                </div>

                                <input class="uk-button uk-button-primary uk-width-1-1" type="submit" value="Submit" name="submit">
                            </fieldset>
                                
                            
                        </form>
                    </div>
                </div>

                <div class="uk-width-1-3@m">
                </div>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>