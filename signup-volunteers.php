<?php
    session_start(); //session start
?>
<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = 
$password = 
$email = 
$firstname = 
$lastname = 
$phone = 
$facebook = 
$address = 
"";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM volunteers WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter your email.";     
    }  else{
        $email = trim($_POST["email"]);
    }

    // Validate
    if(empty(trim($_POST["firstname"]))){    
    }  else{
        $firstname = trim($_POST["firstname"]);
    }
    // Validate
    if(empty(trim($_POST["lastname"]))){    
    }  else{
        $lastname = trim($_POST["lastname"]);
    }
    // Validate
    if(empty(trim($_POST["phone"]))){    
    }  else{
        $phone = trim($_POST["phone"]);
    }
    // Validate
    if(empty(trim($_POST["facebook"]))){    
    }  else{
        $facebook = trim($_POST["facebook"]);
    }
    // Validate
    if(empty(trim($_POST["address"]))){    
    }  else{
        $address = trim($_POST["address"]);
    }
    

    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) ){
        
        // Prepare an insert statement
        $sql = "INSERT INTO volunteers (username, password, email, firstname, lastname, phone, facebook, address)
                VALUES (?, ?, '$email', '$firstname', '$lastname', '$phone', '$facebook', '$address')";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                $_SESSION['success'] = " <div class='uk-padding uk-alert-success uk-text-large uk-text-center' uk-alert>
                                <a class='uk-alert-close' uk-close></a>
                                <p>You have successfully added <span class='uk-text-bold'>$firstname $lastname</span> as one of AW volunteers!</p>
                            </div> ";
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>
    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <!-- notification message -->
            <?php if (isset($_POST['submit'])) : ?>
                <div class="error success" >
                    <h3>
                        <?php 
                            echo $_SESSION['success']; 
                            unset($_SESSION['success']);
                        ?>
                    </h3>
                </div>
            <?php endif ?>

            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Add Volunteer</h1>
            <P class="uk-text uk-text uk-margin-remove-top">Please fill this form to create an account. All fields are required.</P>
            
            <div class="uk-container-padded">
            <form class="uk-form uk-container-padded" name="signup" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                <h3 class="uk-text-bold">Account Details</h3>
                <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                        <input name="username" class="uk-input uk-text-bold" type="text" placeholder="Username" value="<?php echo $username; ?>" required>
                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter username</p>
                        <p class=" uk-alert-danger"><?php echo $username_err; ?></p>
                    </div>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="password" class="uk-input uk-text-bold" type="password" placeholder="Password" value="<?php echo $password; ?>">
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter unique password</p>
                            <p class=" uk-alert-danger"><?php echo $password_err; ?></p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="email" class="uk-input uk-text-bold" type="text" placeholder="Email Address" value="<?php echo $email; ?>" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter a valid email address</p>
                        </div>
                    </div>

                    <h3 class="uk-text-bold">Personal Details</h3>
                    <p>Please fill out all fields.</p>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                        </div>
                    </div>

                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-3@m" >
                            <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter contact number</p>
                        </div>
                        <div class="uk-width-2-3@m" >
                            <input name="facebook" class="uk-input uk-text-bold" type="text" placeholder="Facebook URL" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter facebook URL</p>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <input name="address" class="uk-input uk-text-bold" type="text" placeholder="Enter location">
                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Location</p>
                    </div>

                    <input class="uk-button uk-button-primary uk-width-1-1 uk-margin-top" type="submit" value="Submit" name="submit">
                </fieldset>
                    
                
            </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>