<?php include 'process-signup.php'; ?>
<?php
    session_start(); //session start
    
    if ( !session_id() ) {
        session_start();
    }
    $username = $_SESSION['username'];
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-green uk-margin-remove-bottom">Successfully signed up</h1>
            <h2 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Thank you <?php echo $username; ?> <?php echo $firstname; ?> <?php echo $lastname;; ?></h2>
            <P class="uk-text uk-margin-remove-top">You have successfully signed up to AccessiWheels. </P>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>