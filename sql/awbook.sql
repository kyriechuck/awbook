-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2020 at 10:14 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `awbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'admin@admin.net', '$2y$10$4ranEHi01V6OfrJo5FRs6OlHNjMxPwqlsYnQjtPFPjatmS5UY8kqm', '2020-04-11 15:45:41'),
(15, 'charlesbukingjr', '$2y$10$EaqMwc4KE7RFKbe8FnyoPuW/KWWstzc2STMnCiRnDj4HLhhz0XNkG', '2020-04-12 13:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) NOT NULL,
  `passcode` varchar(10) DEFAULT NULL,
  `category` varchar(80) DEFAULT NULL,
  `bike_status` varchar(10) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `pickup` varchar(255) NOT NULL,
  `dropoff` varchar(255) NOT NULL,
  `date` varchar(80) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `budget` varchar(80) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` varchar(80) DEFAULT NULL,
  `driver_id` int(11) NOT NULL,
  `distance` varchar(80) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `passcode`, `category`, `bike_status`, `firstname`, `lastname`, `phone`, `pickup`, `dropoff`, `date`, `time`, `budget`, `message`, `status`, `driver_id`, `distance`, `created_at`) VALUES
(183, NULL, NULL, NULL, NULL, NULL, NULL, 'NKTI', 'TOWN', NULL, NULL, NULL, NULL, 'pending', 15, NULL, '2020-04-17 03:42:43'),
(184, 'ab1234', 'patient', '', 'henry', 'ford', '12312', 'nkti', 'aim', '1', '2', '3', 'hello', 'pending', 12, '1.5', '2020-04-17 03:55:48'),
(185, 'ab124', 'patient', 'no', 'henry', 'ford', '1231', 'nkti', 'sm', 'na', 'na', '200', 'heto na', 'pending', 5, '1.5', '2020-04-17 04:12:54'),
(186, 'SH8065', 'Frontliner', 'Yes', 'Shelley', 'Hood', '+1 (241) 638-8065', 'Rerum deserunt aut c', 'Vero laborum Sint ', '1970-09-02', '08:00 PM', '74', 'Aut expedita cum num', 'bikerequest', 27, '34', '2020-04-17 13:25:18'),
(187, 'AV3316', 'Patient', 'Yes', 'Austin', 'Vaughan', '+1 (957) 373-3316', 'Et eveniet rerum qu', 'Omnis qui fugiat de', '1988-02-28', '03:00 PM', '45', 'Enim excepteur est o', 'completed', 18, '70', '2020-04-17 13:26:53'),
(188, 'ST8604', 'Frontliner', 'Yes', 'Steven', 'Tillman', '+1 (861) 901-8604', 'Dolore est in tenet', 'Quibusdam minim proi', '1998-08-17', '08:00 PM', '67', 'Et totam iure evenie', 'scheduled', 18, '63', '2020-04-17 13:30:08'),
(189, 'XM3797', 'Patient', 'Yes', 'Xenos', 'Meyer', '+1 (445) 421-3797', 'Quia itaque earum re', 'Ex quia impedit est', '1975-01-23', '01:00 PM', '46', 'Harum placeat labor', 'pending', 24, '85', '2020-04-17 13:32:43'),
(190, 'XM3797', 'Patient', 'Yes', 'Xenos', 'Meyer', '+1 (445) 421-3797', 'Quia itaque earum re', 'Ex quia impedit est', '1975-01-23', '01:00 PM', '46', 'Harum placeat labor', 'pending', 24, '85', '2020-04-17 13:33:50'),
(191, 'GB7129', 'Patient', 'Yes', 'Gareth', 'Briggs', '+1 (667) 921-7129', 'Exercitation volupta', 'Suscipit ut est quia', '2007-04-03', '10:30 AM', '67', 'Tenetur alias labore', 'pending', 21, '6', '2020-04-17 13:34:03'),
(192, 'GB7129', 'Patient', 'Yes', 'Gareth', 'Briggs', '+1 (667) 921-7129', 'Exercitation volupta', 'Suscipit ut est quia', '2007-04-03', '10:30 AM', '67', 'Tenetur alias labore', 'pending', 21, '6', '2020-04-17 13:34:18'),
(193, 'MM7254', 'Patient', 'Yes', 'Martena', 'Miles', '+1 (719) 995-7254', 'Rem autem enim tempo', 'Nihil reprehenderit ', '1997-07-05', '09:30 AM', '26', 'Veritatis culpa quod', 'bikerequest', 27, '5', '2020-04-17 13:35:32'),
(194, 'UW9062', 'Frontliner', 'Yes', 'Upton', 'Weber', '+1 (426) 883-9062', 'Nostrud eius qui con', 'Fugiat omnis est re', '1970-05-22', '11:00 PM', '1', 'Qui consectetur nos', 'pending', 25, '46', '2020-04-17 13:37:27'),
(195, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:38:40'),
(196, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:39:43'),
(197, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:40:15'),
(198, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:40:26'),
(199, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:40:36'),
(200, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:41:51'),
(201, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:42:34'),
(202, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:42:55'),
(203, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:43:06'),
(204, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:43:31'),
(205, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:44:07'),
(206, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:44:15'),
(207, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:45:32'),
(208, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:46:18'),
(209, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:48:42'),
(210, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:49:23'),
(211, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:49:31'),
(212, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:49:46'),
(213, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:50:21'),
(214, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:50:51'),
(215, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:51:35'),
(216, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:51:54'),
(217, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:52:05'),
(218, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:52:42'),
(219, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:52:55'),
(220, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:53:11'),
(221, 'BS1726', 'Frontliner', 'Yes', 'Boris', 'Saunders', '+1 (594) 333-1726', 'In nobis id magnam m', 'Iste tempor beatae a', '1976-02-13', '09:00 PM', '85', 'Dolor officia sed ve', 'pending', 20, '65', '2020-04-17 13:53:33'),
(222, 'AT4254', 'Frontliner', 'Yes', 'Alfreda', 'Terrell', '+1 (304) 942-4252', 'Sunt tempora eius eo', 'Ex et aperiam mollit', '1978-09-22', '10:30 PM', '92', 'Facere rerum hic quo', 'pending', 27, '54', '2020-04-17 13:53:57'),
(223, 'AL8564', 'Frontliner', 'Yes', 'Aaron', 'Lindsey', '+1 (645) 398-8564', 'Molestiae ut aut odi', 'Excepteur ut impedit', '2001-05-05', '12:00 PM', '89', 'Rem veniam libero e', 'pending', 14, '1.3', '2020-04-17 14:03:21'),
(224, 'AL8564', 'Frontliner', 'Yes', 'Aaron', 'Lindsey', '+1 (645) 398-8564', 'Molestiae ut aut odi', 'Excepteur ut impedit', '2001-05-05', '12:00 PM', '89', 'Rem veniam libero e', 'pending', 14, '1.3', '2020-04-17 14:04:09'),
(225, 'KB7976', 'Frontliner', 'Yes', 'Kevyn', 'Bass', '+1 (949) 561-7976', 'Dolor nostrum corpor', 'Irure nihil illo ame', '1984-05-01', '07:00 PM', '11', 'Labore sit quasi mol', 'pending', 22, '50', '2020-04-17 14:18:03'),
(226, 'HL6164', 'Patient', 'Yes', 'Hilda', 'Levy', '+1 (762) 549-6164', 'Ipsum excepturi ven', 'Possimus magnam mol', '1973-03-28', '04:00 PM', '19', 'Porro ipsum itaque ', 'Cancelled by Driver', 20, '74', '2020-04-17 14:19:16'),
(227, 'HL6164', 'Patient', 'Yes', 'Hilda', 'Levy', '+1 (762) 549-6164', 'Ipsum excepturi ven', 'Possimus magnam mol', '1973-03-28', '04:00 PM', '19', 'Porro ipsum itaque ', 'Cancelled by Driver', 20, '74', '2020-04-17 14:19:47'),
(228, 'LM1946', 'Patient', 'Yes', 'Lavinia', 'Madden', '+1 (953) 705-1946', 'In incidunt explica', 'Praesentium magnam r', '2019-02-05', '02:30 PM', '21', 'Eum aliquid veniam ', 'pending', 18, '51', '2020-04-17 14:34:54'),
(229, 'RM1305', 'Frontliner', 'Yes', 'Rashad', 'Moore', '+1 (901) 444-1305', 'Rerum consectetur n', 'Numquam fugit aut v', '1970-03-22', '02:00 PM', '61', 'Sed sunt ullam nemo ', 'pending', 20, '86', '2020-04-17 15:02:44'),
(230, 'HF9034', 'Frontliner', 'Yes', 'Hu', 'Frank', '+1 (494) 947-9034', 'Officiis veritatis e', 'Vel asperiores fugia', '2018-01-26', '12:00 AM', '61', 'Voluptates neque inc', 'pending', 25, '69', '2020-04-17 15:48:24'),
(231, 'IW8406', 'Frontliner', 'Yes', 'Isadora', 'Wallace', '+1 (767) 625-8406', 'Mollit corporis magn', 'Impedit do voluptat', '1995-04-05', '07:30 PM', '40', 'Irure non beatae est', 'bikerequest', 17, '56', '2020-04-17 15:53:21'),
(232, 'db1234', 'patient', 'Yes', 'charles', 'buking', '091512312', 'teachers village', 'malakas st', '1991-08-30', '10:00', '80', 'paantay 5min', 'pending', 11, '1.5', '2020-04-17 15:53:50'),
(233, 'FB2974', 'Patient', 'Yes', 'Freya', 'Bright', '+1 (118) 624-2974', 'Aut lorem tempor ips', 'Autem dicta sit eni', '1987-10-31', '11:30 AM', '93', 'Dolores qui consequa', 'received', 26, '1', '2020-04-17 18:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(80) NOT NULL,
  `address` varchar(255) NOT NULL,
  `cartype` varchar(100) NOT NULL,
  `carmodel` varchar(80) DEFAULT NULL,
  `facebook` varchar(150) DEFAULT NULL,
  `availability` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `firstname`, `lastname`, `phone`, `address`, `cartype`, `carmodel`, `facebook`, `availability`, `status`, `created_at`) VALUES
(14, 'Oscar', 'Reyes', '09158823932', 'Marikina City', 'sedan', 'Vios 2015', NULL, NULL, 'status', '2020-04-13 23:33:54'),
(17, 'Sheila', 'Cohen', '+1 (885) 488-1116', 'Dolores qui repudian', 'van', 'Quia quia esse id in', NULL, NULL, 'active', '2020-04-14 15:00:11'),
(18, 'Elmo', 'Hurst', '+1 (494) 324-2912', 'Blanditiis obcaecati', 'van', 'Autem excepteur qui', NULL, NULL, 'inactive', '2020-04-14 15:05:58'),
(19, 'Hilel', 'Matthews', '+1 (566) 186-3955', 'Nisi ut ut laboris c', 'auv', 'Officia ipsa est ve', 'Delectus nostrum ve', 'Accusamus provident', 'warning', '2020-04-14 15:27:33'),
(20, 'Colleen', 'Brennan', '+1 (231) 745-7905', 'Obcaecati beatae non', 'van', 'Exercitation dicta m', 'Sint mollitia sed et', 'Est nobis eligendi e', 'inactive', '2020-04-14 15:30:19'),
(21, 'Marshall', 'Knight', '+1 (363) 659-3089', 'Enim maxime nemo aut', 'van', 'Consequuntur quis re', 'Quod laudantium omn', 'Voluptates expedita', 'inactive', '2020-04-14 17:01:54'),
(22, 'juan', 'delacruz', '13124', 'qc', 'sedan', 'vios', 'facebok.com', 'monday', 'active', '2020-04-14 17:03:55'),
(23, 'Cassady', 'Phelps', '+1 (974) 245-7058', 'Dolorum nulla volupt', 'ambulance', 'Toyota HiAce', 'Harum sit temporibu', 'Voluptatem praesenti', 'active', '2020-04-14 17:49:54'),
(24, 'Indira', 'Vaughan', '+1 (153) 957-9004', 'Pariatur Voluptas e', 'ambulance', 'Anim eos tempore cu', 'Non minim adipisci i', 'Fuga Voluptatem Mo', 'warning', '2020-04-14 17:53:11'),
(25, 'Oleg', 'Long', '+1 (692) 444-4364', 'Ullam sit vel quod e', '', '', 'Quia laboris magnam', 'Eos non est nemo tem', 'inactive', '2020-04-14 19:53:56'),
(26, 'Wanda', 'Todd', '+1 (329) 445-3927', 'Sit nulla culpa vel', 'van', 'Urvan', 'Quas fuga Placeat', 'Consequat Illum es', 'active', '2020-04-14 20:08:08'),
(27, 'Buffy', 'Blackburn', '+1 (502) 315-1486', 'Beatae nobis officii', 'auv', 'Deserunt lorem rerum', 'Earum recusandae Di', 'Accusamus ad volupta', 'inactive', '2020-04-14 21:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'nagewuk', '$2y$10$ultZNmN/fx50fq/GSz9QfO3MAfVqoCFPCH45j/hmW36aQCBOhQx4G', '2020-04-12 05:38:08'),
(2, 'lazoseqa', '$2y$10$WmGp6IB4S.E.W538uS3ZnOvW.PdIGxbJjTPLll79a9F2NE1LZu1IG', '2020-04-12 05:40:36'),
(3, 'dofiqa', '$2y$10$eTPh130B5uHq8kluIQw/Feg7W.8tdImDrPGqXBJ5DwAJVD1wVEpOm', '2020-04-12 05:43:00'),
(4, 'demo@demo.com', '$2y$10$B6u/AMSUof3if3dIHXwIh.P31hXuk8LC8jYnsUVoFcrMgJwqhkqpq', '2020-04-12 05:44:04'),
(5, 'charlesbukingjr', '$2y$10$WPGj7U0omyqNNDnnyxR3weENuYCfyn3pwTKZDRZrXo6RFHOB8XGZy', '2020-04-13 20:18:45'),
(6, 'buwiwyti', '$2y$10$Rk2KFlXVoHBopxNPCQyAX.pOo1MEgH1gcd3v.1k64HtSOvVm1fa5S', '2020-04-16 21:26:08');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(80) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `phone`, `facebook`, `address`, `created_at`) VALUES
(12, 'bykuw', '$2y$10$HgT3ZWBjdnhMXpT9OjzyIuHVGKLAY3CrQsyckchloFluO.5cx7awi', 'ryvune@mailinator.net', 'Hashim', 'Salinas', '+1 (734) 827-8543', 'Aliquam earum vel ul', 'Veniam nulla except', '2020-04-16 22:04:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
