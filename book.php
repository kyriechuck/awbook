
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        
        <title>Book a Ride - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>


    <!-- start main section -->
    <div class="uk-section uk-section-lightbackground">
		<div class="uk-container uk-container-small">
            <div class="uk-alert-success" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <h3 class="uk-text-bold">Reminders to Passengers</h3>
                <p class="uk-text"></p>
                <p >
                    <ol >
                        <li>Booking must be <strong>2 DAYS IN ADVANCE</strong></li>
                        <li>You can monitor RIDE STATUS using a Passenger Code</li>
                        <li>You can also contact Facebook page of AccessiWheels. </li>
                        <li>A standard rate of Php200 per trip for fixed 10 kilometers is available for subscription.</li>
                        <li><strong>GUARANTEED SERVICE</strong> for a sedan car within 8 kilometers starts at Php 430 and for a 6-hour rent, single destination is Php 1,660.</li>
                        <li>Passengers are still welcome to input any amount so they may be covered by <strong>SAGOT KO RIDE MO initiative</strong> </li>
                        <li>You will know the details of available driver, a day before your trip.</li>
                        <li>You have the right to cancel the right upon receiving the details of the driver <strong>BUT NOT ON THE SAME DAY OF SERVICE.</strong></li>
                        <li>Payments are encouraged to be paid in advance. Passengers can also ask the designated AccessiWheels Ambassador in selected center/office for payment processing.</li>
                        <li>Bring medical appointment/senior citizen/person with disability ID if patients.</li>
                        <li>Bring company ID as health worker or employee.</li>
                        <li>Please respect time.</li>


                    </ol>
                </p>
            </div>


            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">All fields are required.</P>
            
            <div class="uk-grid ">    
                <div class="uk-width-2-3@m" >
                    <div class="uk-container ">
                        <form class="uk-form uk-container-padded" name="book" action="book-pudo.php" method="POST">
                            <fieldset class="uk-fieldset">
                                <p class="uk-text uk-margin-remove-top">Select a category.</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                    <label class="uk-text-bold"><input class="uk-checkbox" type="radio" name="category" value="Patient" <?=($category == "patient" ? "checked": "") ?> checked>  Patient</label>
                                    <label class="uk-text-bold" ><input class="uk-checkbox" type="radio" name="category" value="Frontliner" <?=($category == "frontliner" ? "checked": "") ?> > Frontliner</label>
                                </div>
                                
                            </fieldset>
                            <input class="uk-button uk-button-primary uk-width-1-1" type="submit" value="Proceed" name="submit">
                        </form>
                    </div>
                </div>

                <div class="uk-width-1-3@m">
                </div>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>