<?php

session_start(); //session start

// Include config file
require_once "config.php";

// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM volunteers WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter a password.";     
    }  else{
        $email = trim($_POST["email"]);
    }


    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $phone = trim($_POST["phone"]);
    $address = trim($_POST["address"]);
    $facebook = trim($_POST["facebook"]);


    // Prepare an update statement
    $sql = "UPDATE volunteers SET username='$username', password='$password', email='$email', firstname='$firstname', lastname='$lastname', phone='$phone', address='$address', facebook='$facebook' WHERE id=$id ";
        
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "sssi", $param_username, $param_password, $param_email, $param_firstname, $param_lastname,  $param_phone, $param_address, $param_facebook, $param_id);
        
        // Set parameters
        $param_username = $username;
        $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
        $param_email = $email;
        $param_firstname = $firstname;
        $param_lastname = $lastname;
        $param_phone = $phone;
        $param_address = $addres;
        $param_facebook = $facebook;
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records updated successfully. Redirect to landing page
            $_SESSION['success'] = " <div class='uk-padding uk-alert-success uk-text-large uk-text-center' uk-alert>
                                <a class='uk-alert-close' uk-close></a>
                                <p>You have successfully updated the details of volunteer <span class='uk-text-bold'>$firstname $lastname</span> !</p>
                            </div> ";
        } else{
            echo "Something went wrong. Please try again later.";
        }
    }
        
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM volunteers WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $id = $row["id"];
                    $username = $row['username'];
                    $password = $row['password'];
                    $email = $row['email'];
                    $firstname = $row["firstname"];
                    $lastname = $row['lastname'];
                    $name = $firstname .' '. $lastname;
                    $phone = $row["phone"];
                    $address = $row["address"];
                    $facebook = $row['facebook'];
                    $created_atold = $row["created_at"];
                    $created_at = date("M, d, Y", strtotime($created_atold));
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                <?php if (isset($_POST['submit'])) : ?>
                    <div class="error success" >
                        <h3>
                            <?php  echo $_SESSION['success']; ?>
                        </h3>
                    </div>
                <?php endif ?>
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>
                <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Update Volunteer Details</h1>
                <P class="uk-text uk-text uk-margin-remove-top">Edit values then submit to update driver details.</P>
                
                <form class="uk-form uk-container-padded" name="signup" action="admin-volunteer-update.php" method="POST">
                    <h3 class="uk-text-bold">Account Details</h3>
                    <fieldset class="uk-fieldset">
                        <div class="uk-margin">
                            <input name="username" class="uk-input uk-text-bold" type="text" placeholder="Username" value="<?php echo $username; ?>" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter username</p>
                            <p class=" uk-alert-danger"><?php echo $username_err; ?></p>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="password" class="uk-input uk-text-bold" type="password" placeholder="Password" value="<?php echo $password; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter unique password</p>
                                <p class=" uk-alert-danger"><?php echo $password_err; ?></p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="email" class="uk-input uk-text-bold" type="text" placeholder="Email Address" value="<?php echo $email; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter a valid email address</p>
                            </div>
                        </div>

                        <h3 class="uk-text-bold">Personal Details</h3>
                        <p>Please fill out all fields.</p>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="firstname" class="uk-input uk-text-bold" value="<?php echo $firstname; ?>" type="text" placeholder="First Name" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="lastname" class="uk-input uk-text-bold" type="text" value="<?php echo $lastname; ?>" placeholder="Last Name" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                            </div>
                        </div>

                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="phone" class="uk-input uk-text-bold" type="text" value="<?php echo $phone; ?>" placeholder="Contact Number" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter contact number</p>
                            </div>
                            <div class="uk-width-2-3@m" >
                                <input name="facebook" class="uk-input uk-text-bold" type="text" value="<?php echo $facebook; ?>" placeholder="Facebook URL">
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter facebook URL</p>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <input name="address" class="uk-input uk-text-bold" type="text" value="<?php echo $address; ?>" placeholder="Enter location">
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Location</p>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input class="uk-button uk-button-primary uk-width-1-1 uk-margin-top" type="submit" value="Update" name="submit">
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>
