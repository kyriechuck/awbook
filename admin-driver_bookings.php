<?php
    session_start(); //session start
    
    // Include config file
    require_once "config.php";
    
    // Attempt select query execution
    $sql = "SELECT 
        bookings.firstname as bookingFirstname, 
        bookings.lastname as bookingLastname, 
        bookings.category,
        bookings.passcode,
        bookings.pickup,
        bookings.dropoff,
        bookings.date as bookingDate,
        bookings.time,
        bookings.created_at as bookingCreatedat,
        bookings.status as bookingStatus,
        drivers.firstname as driverFirstname,
        drivers.lastname as driverLastname
        FROM drivers JOIN bookings on drivers.id = bookings.driver_id WHERE drivers.id =? ORDER by date DESC";
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>



    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container">

            <div class="uk-grid uk-margin">
                <div class="uk-width-1-2@m">
                    <p><a href="admin-drivers.php">Go back to list of drivers. </a></p>
                    <h3 class="uk-margin-remove-top uk-text-bold uk-text-purple">Below are all the listed bookings of the selected driver.</h3>
                </div>
            </div>

            <!-- main content -->
            <div class="uk-container-padded">
                
                <div class="uk-overflow-auto">
                    <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                        <thead>
                                <tr>
                                <th class="uk-width-small">Driver</th>
                                <th class="uk-width-small" >Passenger</th>
                                <th class="uk-width-small">Pick-up Point</th>
                                <th class="uk-width-small">Drop-off Point</th>
                                <th class="uk-width-small" >PU Date and Time</th>
                                <th class="uk-width-small">Date Added</th>
                                <th class="uk-table-shrink">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // Check existence of id parameter before processing further
                            if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
                                
                                if($stmt = mysqli_prepare($link, $sql)){
                                    // Bind variables to the prepared statement as parameters
                                    mysqli_stmt_bind_param($stmt, "i", $param_id);
                                    
                                    // Set parameters
                                    $param_id = trim($_GET["id"]);


                                    
                                    // Attempt to execute the prepared statement
                                    if(mysqli_stmt_execute($stmt)){
                                        $result = mysqli_stmt_get_result($stmt);
                                
                                        if(mysqli_num_rows($result) > 0){
                                            /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                                            while($row = mysqli_fetch_array($result)){
                                                $id = $row["id"];
                                                $passcode = $row["passcode"];
                                                $category = $row["category"];
                                                $name = $row["bookingFirstname"] ." ". $row['bookingLastname'];
                                                $dropoff = $row["dropoff"];
                                                $pickup = $row["pickup"];
                                                $date_old = $row["bookingDate"];
                                                $date = date("M, d, Y", strtotime($date_old));
                                                $time = $row["time"];
                                                $status = $row["bookingStatus"];
                                                $date_added_old = $row["bookingCreatedat"];
                                                $date_added = date("M, d, Y", strtotime($date_added_old));

                                                $drivername = $row['driverFirstname'] .' '. $row['driverLastname'];


                                                echo '<tr> 
                                                        <td>'.$drivername.'</td>
                                                        <td class="uk-text-wrap">'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='admin-ride-update.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $category.'</td>
                                                        <td class="uk-text-wrap">'.$pickup.'</td>  
                                                        <td class="uk-text-wrap">'.$dropoff.'</td>
                                                        <td class="uk-text-wrap">'.$date. '<br>' . $time .'</td>
                                                        <td class="uk-text-wrap">'.$date_added.'</td>  
                                                        <td class="uk-text-wrap uk-text-capitalize">' .$status . '</td> 
                                                    </tr>';
                                            }

                                        } else{
                                            // URL doesn't contain valid id parameter. Redirect to error page
                                            header("location: error.php");
                                            exit();
                                        }
                                        
                                    } else{
                                        echo "Oops! Something went wrong. Please try again later.";
                                    }
                                }
                                 
                                // Close statement
                                mysqli_stmt_close($stmt);
                                
                                // Close connection
                                mysqli_close($link);
                            } else{
                                // URL doesn't contain id parameter. Redirect to error page
                                header("location: error.php");
                                exit();
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Drivers</a></li>
                <li><a href="#">Driver's Bookings</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>