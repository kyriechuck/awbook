<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            
             <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">Drivers</h1>
                    <p>Below are all the listed partner drivers of AccessiWheels</p>
                </div>
                <div class="uk-width-1-2@m ">
                    <a href="admin-add-driver.php" class="uk-button uk-button-primary uk-align-right">Add New Driver</a>
                </div>
            </div>
            
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <form class="uk-search uk-search-default uk-align-left">
                        <span uk-search-icon></span>
                        <input class="uk-search-input uk-search-large uk-text-small " type="search" placeholder="Search Driver">
                    </form>
                </div>
                <div class="uk-width-1-3@m">
                    <div class="uk-margin uk-align-right">
                        <button class="uk-button uk-button-default uk-text-right" type="button">Display: 5 <span uk-icon="triangle-down"></span></button>
                        <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="#">5</a></li>
                                <li><a href="#">10</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">25</a></li>
                                <li><a href="#">50</a></li>
                                <li><a href="#">100</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                <a href="#"><p class="uk-text-small">Go Back</p></a>

                <div class="uk-grid ">
                    <div class="uk-width-4-5@m">
                        <h3 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Juan Dela Cruz</h3>
                        <p class="uk-text uk-margin">Date Added: April 10, 2020</p>
                    </div>
                    <div class="uk-width-1-5@m uk-text-right@m">
                        <p class="uk-margin-remove-bottom uk-text-small uk-text-bold">STATUS</p>
                        <div class="uk-margin">
                            <select class="uk-select">
                                <option>Active</option>
                                <option>Inactive</option>
                                <option>Deavtivated</option>
                            </select>
                        </div>
                    </div>
                    
                </div>
                
                <hr>
                
                <div class="uk-margin uk-width-auto">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">ADDRESS</p>
                            <h4 class="uk-margin-small uk-text-purple">Maalalahanin, Teachers Village, Quezon City</h4>
                        </div>
                    </div>
                    <hr>
                    <div class="uk-grid ">
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">CONTACT NUMBER</p>
                            <h4 class="uk-margin-small ">091599230823</h4>
                        </div>
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">EMAIL ADDRESS</p>
                            <h4 class="uk-margin-small">driver@gmail.com</h4>
                        </div>
                        <div class="uk-width-1-3@m">
                            <p class=" uk-margin-remove-bottom uk-text-small uk-text">CAR TYPE</p>
                            <h4 class="uk-margin-small ">Sedan</h4>
                        </div>
                    </div>

                    <div class="uk-margin-large-top">
                        <hr>
                        <a class="uk-button uk-button-default uk-padding-small uk-text-small">Back to List</a>
                        <a class="uk-button uk-button-primary uk-padding-small uk-text-small">Save Changes</a>
                    </div>
                </div>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>