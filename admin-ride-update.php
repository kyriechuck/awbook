<?php

session_start(); //session start

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
// Processing form data when form is submitted
// Check input errors before inserting in database
// Prepare an update statement
// Bind variables to the prepared statement as parameters
// Set parameters
// Attempt to execute the prepared statement
// Close statement
// Close connection


    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM bookings WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $id = $row["id"];
                    $category = $row["category"];
                    $passcode = $row["passcode"];
                    $firstname = $row["firstname"];
                    $lastname = $row['lastname'];
                    $phone = $row["phone"];
                    $email = $row["email"];
                    $pickup = $row["pickup"];
                    $dropoff = $row["dropoff"];
                    $date_old = $row["date"];
                    $date = date("M, d, Y", strtotime($date_old));
                    $time = $row["time"];
                    $message = $row["message"];
                    $budget = $row["budget"];
                    $driver_id = $row["driver_id"];
                    $distance = $row["distance"];
                    $status = $row["status"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }

// end update ride


?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                <?php if (isset($_POST['submit'])) : ?>
                    <div class="error success" >
                        <h3>
                            <?php  echo $_SESSION['success-ride']; ?>
                        </h3>
                    </div>
                <?php endif ?>
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>
                <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom uk-margin-remove-top">Update Ride Details for <?php echo $firstname . ' '. $lastname ; ?></h1>
                <P class="uk-text uk-text uk-margin-remove-top">Edit values then submit to update this booking request.</P>
                
                <form class="uk-form uk-container-padded"  action="admin-update-ride_submitted.php" method="POST">
                     
                    <fieldset class="uk-fieldset">

                        <div class="uk-grid uk-margin">
                            <div class="uk-width-2-5@m" >
                                <label>First Name</label>
                                <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" value="<?php echo $firstname; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">First Name</p>
                            </div>
                            <div class="uk-width-2-5@m" >
                                <label>Last Name</label>
                                <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $lastname; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Last Name</p>
                            </div>
                            <div class="uk-width-1-5@m" >
                                <label>Passenger Code</label>
                                <input name="passcode" class="uk-input uk-text-bold" type="text" placeholder="Passenger Code" value="<?php echo $passcode; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Passenger Code</p>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <label>Category: Frontliner / Patient</label>
                                <input name="category" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $category; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Category: Frontliner / Patient</p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <label>Contact Number</label>
                                <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" value="<?php echo $phone; ?>"  >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Contact Number</p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <label>Email Address</label>
                                <input name="email" class="uk-input uk-text-bold" type="email" placeholder="Email Address" value="<?php echo $email; ?>"  >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Email Address</p>
                            </div>
                        </div>
                        <hr>
                        <div class="uk-margin uk-width-auto">
                            <label>Pick-up Point</label>
                            <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="Pick-up Point" value="<?php echo $pickup; ?>" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Pick-up Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <label>Drop-off Point</label>
                            <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="Drop-off Point" value="<?php echo $dropoff; ?>" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Drop-off Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                    
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <label>Pick-up Date</label>
                                <input name="date" class="uk-input" type="date" placeholder="" value="<?php echo $date_old; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <label>Pick-up Time</label>
                                <select name="time" class="uk-select" value="<?php echo $time; ?>" >
                                    <option value="05:00 AM" <?=($time == "05:00 AM" ? "selected": "") ?>>05:00 AM</option>
                                    <option value="05:30 AM" <?=($time == "05:30 AM" ? "selected": "") ?>>05:30 AM</option>
                                    <option value="06:00 AM" <?=($time == "06:00 AM" ? "selected": "") ?>>06:00 AM</option>
                                    <option value="06:30 AM" <?=($time == "06:30 AM" ? "selected": "") ?>>06:30 AM</option>
                                    <option value="07:00 AM" <?=($time == "07:00 AM" ? "selected": "") ?>>07:00 AM</option>
                                    <option value="07:30 AM" <?=($time == "07:30 AM" ? "selected": "") ?>>07:30 AM</option>
                                    <option value="08:00 AM" <?=($time == "08:00 AM" ? "selected": "") ?>>08:00 AM</option>
                                    <option value="08:30 AM" <?=($time == "08:30 AM" ? "selected": "") ?>>08:30 AM</option>
                                    <option value="09:00 AM" <?=($time == "09:00 AM" ? "selected": "") ?>>09:00 AM</option>
                                    <option value="09:30 AM" <?=($time == "09:30 AM" ? "selected": "") ?>>09:30 AM</option>
                                    <option value="10:00 AM" <?=($time == "10:30 AM" ? "selected": "") ?>>10:00 AM</option>
                                    <option value="10:30 AM" <?=($time == "10:30 AM" ? "selected": "") ?>> 10:30 AM</option>
                                    <option value="11:00 AM" <?=($time == "11:00 AM" ? "selected": "") ?>>11:00 AM</option>
                                    <option value="11:30 AM" <?=($time == "11:30 AM" ? "selected": "") ?>>11:30 AM</option>
                                    <option value="12:00 PM" <?=($time == "12:00 PM" ? "selected": "") ?>>12:00 PM</option>
                                    <option value="12:30 PM" <?=($time == "12:30 PM" ? "selected": "") ?>>12:30 PM</option>
                                    <option value="01:00 PM" <?=($time == "01:00 PM" ? "selected": "") ?>>01:00 PM</option>
                                    <option value="01:30 PM" <?=($time == "01:30 PM" ? "selected": "") ?>>01:30 PM</option>
                                    <option value="02:00 PM" <?=($time == "02:00 PM" ? "selected": "") ?>>02:00 PM</option>
                                    <option value="02:30 PM" <?=($time == "02:30 PM" ? "selected": "") ?>>02:30 PM</option>
                                    <option value="03:00 PM" <?=($time == "03:00 PM" ? "selected": "") ?>>03:00 PM</option>
                                    <option value="03:30 PM" <?=($time == "03:30 PM" ? "selected": "") ?>>03:30 PM</option>
                                    <option value="04:00 PM" <?=($time == "04:00 PM" ? "selected": "") ?>>04:00 PM</option>
                                    <option value="04:30 PM" <?=($time == "04:30 PM" ? "selected": "") ?>>04:30 PM</option>
                                    <option value="05:00 PM" <?=($time == "05:00 PM" ? "selected": "") ?>>05:00 PM</option>
                                    <option value="05:30 PM" <?=($time == "05:30 PM" ? "selected": "") ?>>05:30 PM</option>
                                    <option value="06:00 PM" <?=($time == "06:00 PM" ? "selected": "") ?>>06:00 PM</option>
                                    <option value="06:30 PM" <?=($time == "06:30 PM" ? "selected": "") ?>>06:30 PM</option>
                                    <option value="07:00 PM" <?=($time == "07:00 PM" ? "selected": "") ?>>07:00 PM</option>
                                    <option value="07:30 PM" <?=($time == "07:30 PM" ? "selected": "") ?>>07:30 PM</option>
                                    <option value="08:00 PM" <?=($time == "08:00 PM" ? "selected": "") ?>>08:00 PM</option>
                                    <option value="08:30 PM" <?=($time == "08:30 PM" ? "selected": "") ?>>08:30 PM</option>
                                    <option value="09:00 PM" <?=($time == "09:00 PM" ? "selected": "") ?>>09:00 PM</option>
                                    <option value="09:30 PM" <?=($time == "09:30 PM" ? "selected": "") ?>>09:30 PM</option>
                                    <option value="10:00 PM" <?=($time == "10:00 PM" ? "selected": "") ?>>10:00 PM</option>
                                    <option value="10:30 PM" <?=($time == "10:30 PM" ? "selected": "") ?>> 10:30 PM</option>
                                    <option value="11:00 PM" <?=($time == "11:00 PM" ? "selected": "") ?>>11:00 PM</option>
                                    <option value="11:30 PM" <?=($time == "11:30 PM" ? "selected": "") ?>>11:30 PM</option>

                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Select Time</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <label>Budget</label>
                                <input name="budget" class="uk-input uk-text-bold" type="number" value="<?php echo $budget; ?>">
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Budget</p>
                            </div>
                        </div> 
                        <div class="uk-grid uk-margin">
                            
                            <div class="uk-width-1-3@m" >
                                <label>Status</label>
                                <select name="status" class="uk-select uk-alert-warning" value="<?php echo $status; ?>">
                                    <option value="pending" <?=($status == "pending" ? "selected": "") ?> >Pending</option>
                                    <option value="received" <?=($status == "received" ? "selected": "") ?> >Received</option>
                                    <option value="scheduled" <?=($status == "scheduled" ? "selected": "") ?> >Scheduled</option>
                                    <option value="completed" <?=($status == "completed" ? "selected": "") ?> >Completed</option>
                                    <option value="bikerequest" <?=($status == "bikerequest" ? "selected": "") ?> hidden> Bike Request</option>
                                    <option value="bikelent" <?=($status == "bikelent" ? "selected": "") ?> hidden> Bike Lent</option>
                                    <option value="bikereturned" <?=($status == "bikereturned" ? "selected": "") ?> hidden> Bike Returned</option>
                                    <option value="Cancelled by Driver" <?=($status == "cancelleddriver" ? "selected": "") ?> >Cancelled by Driver</option>
                                    <option value="Cancelled by Rider" <?=($status == "cancelledrider" ? "selected": "") ?> >Cancelled by Rider</option>

                                    <option value="NAD" <?=($status == "NAD" ? "selected": "") ?> >No Available Drivers</option>
                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Status</p>
                            </div>

                            <div class="uk-width-1-3@m" >
                                <label>Assigned Driver</label>
                                <select class="uk-select" name="driver_id" value="<?php echo $driver_id; ?>" >
                                    <?=
                                        // Attempt select query execution
                                        $sql = "SELECT * from drivers";

                                        if($result = mysqli_query($link, $sql)){
                                            if(mysqli_num_rows($result) > 0){

                                                while($row = mysqli_fetch_assoc($result)){

                                                    echo '<option value="'.$row["id"].'" >'.$row["firstname"].' '.$row["lastname"].'</option>';
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        } 
                                    
                                    // Close connection
                                    mysqli_close($link);
                                    ?>
                                    
                                </select>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Assigned Driver</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <label>Trip Distance in kilometers</label>
                                <input name="distance" class="uk-input uk-text-bold uk-alert-success" type="number" step="0.1" value="<?php echo $distance; ?>" >
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Trip Distance in kilometers</p>
                            </div>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <label>Additional Message</label>
                            <textarea name="message" class="uk-textarea" name="message" rows="10" value="" placeholder="Enter your message"><?php echo $message; ?></textarea>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please enter any additional instructions or special request in the box above. E.g. Return Time, Need of vehicle going back to the hospital, etc.</p>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                        <input class="uk-button uk-button-primary uk-margin-top uk-width-1-1" type="submit" value="Update" name="submit">
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Rides</a></li>
                <li><a href="#">Update Ride</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>
