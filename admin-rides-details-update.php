<?php
    session_start(); //session start
?>

<?php
 // Include config file
 require_once "config.php";

// Define variables and initialize with empty values
$budget = $status = "";
$budget_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Validate status
    $input_budget = trim($_POST["budget"]);
    if(empty($input_budget)){
        $budget_err = "Plese input budget";     
    } else{
        $budget = $input_budget;
    }

    // Validate status
    $input_status = trim($_POST["status"]);
    if(empty($input_status)){
        $status_err = "Plese input status";     
    } else{
        $status = $input_status;
    }
    
    // Check input errors before inserting in database
    if(empty($status_err) && empty($budget_err)){

        // Prepare an update statement
        $sql = "UPDATE bookings SET budget=$budget,status=$status WHERE id=$id";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssi", $param_budget, $param_id, $param_status);
            
            // Set parameters
            $param_status = $status;
            $param_budget = $budget;
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                header("location: admin-rides_pending.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM bookings WHERE id = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $id = $row["id"];
                    $name = $row["firstname"] ." ". $row['lastname'];
                    $phone = $row["phone"];
                    $pickup = $row["pickup"];
                    $dropoff = $row["dropoff"];
                    $date = $row["date"];
                    $time = $row["time"];
                    $message = $row["message"];
                    $budget = $row["budget"];
                    $status = $row["status"];
                    $date_added = $row["created_at"];

                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <h1 class="uk-text-bold uk-text-purple">Rides</h1>
                </div>
                <div class="uk-width-1-3@m">
                    <button class="uk-button uk-button-primary uk-align-right">Add New Ride</button>
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li class="uk-active"><a href="admin-rides_pending.php">Pending Rides</a></li>
                        <li ><a href="admin-rides_scheduled.php">Scheduled Rides</a></li>
                        <li><a href="admin-rides_finished.php">Finished Rides</a></li>
                        <li><a href="admin-rides_cancelled.php">Cancelled Rides</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-3@m ">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <form class="uk-search uk-search-default uk-align-right">
                                <span uk-search-icon></span>
                                <input class="uk-search-input uk-text-small " type="search" placeholder="Search Rides">
                            </form>
                        </div>
                        <div class="uk-width-1-2@m">
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p>These are rides which are not yet confirmed and need to be assigned to a driver.</p>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>

                <form class="uk-form" action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                    <div class="uk-grid ">
                        <div class="uk-width-3-5@m">
                            <h3 class="uk-text-bold uk-text-purple uk-margin-remove-bottom"><?php echo $name; ?> </h3>
                            <p class="uk-text uk-margin-remove-top">Passenger Code: JD9332</p>
                            <p class="uk-text uk-margin-remove-top">Date Added: <?php echo $date_added; ?></p>
                        </div>
                        <div class="uk-width-1-5@m uk-text-right@m">
                            <p class="uk-margin-remove-bottom uk-text-small uk-text-bold">ASSIGNED DRIVER</p>
                            <div class="uk-margin">
                                <select class="uk-select">
                                    <option>Select Driver</option>
                                    <option>Melvin Dela Cruz</option>
                                    <option>Oscar Reyes</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-1-5@m uk-text-right@m">
                            <p class="uk-margin-remove-bottom uk-text-small uk-text-bold">STATUS</p>
                            <div class="uk-margin uk-width-auto">
                                <input name="status" class="uk-input uk-text-bold" type="text" placeholder="Budget" value="<?php echo $status; ?>">
                            </div>
                        </div>
                        
                    </div>
                    
                    <hr>
                    
                    <div class="uk-margin uk-width-auto">
                        <div class="uk-grid">
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP POINT</p>
                                <h4 class="uk-margin-small uk-text-purple"><?php echo $row["pickup"]; ?></h4>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">DROP-OFF POINT</p>
                                <h4 class="uk-margin-small uk-text-purple"><?php echo $row["dropoff"]; ?></h4>
                            </div>
                            
                        </div>
                        <hr>
                        <div class="uk-grid ">
                            <div class="uk-width-1-3@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP TIME</p>
                                <h4 class="uk-margin-small "><?php echo $row["time"]; ?></h4>
                            </div>
                            <div class="uk-width-1-3@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP DATE</p>
                                <h4 class="uk-margin-small"><?php echo $row["date"]; ?></h4>
                            </div>
                            <div class="uk-width-1-3@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">CAR TYPE</p>
                                <h4 class="uk-margin-small ">N/A</h4>
                            </div>
                        </div>
                        <hr>
                        <div class="uk-grid">
                            <div class="uk-width-2-3@m">
                                <p class=" uk-margin uk-text-small uk-text-bold">MESSAGE TO DRIVER</p>
                                <p class="uk-margin-small"><?php echo $row["message"]; ?></p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">BUDGET</p>
                                <input name="budget" class="uk-input uk-text-bold" type="number" placeholder="Budget" value="<?php echo $budget; ?>">
                            </div>
                        </div>

                        <div class="uk-margin">
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <hr>
                            <a class="uk-button uk-button-danger uk-padding-small uk-text-small">Back to List</a>
                            <input type="submit" class="uk-button uk-button-primary uk-padding-small uk-text-small" value="Save">
                        </div>
                    </div>
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>