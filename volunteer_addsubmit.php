<?php 
    session_start(); //session start
  
    
    // Include config file
    include "config.php";
  
        // initializing variables
        $passcode = "";
        $category = "";
        $bike_status = "";
        $firstname = ""; 
        $lastname = "";
        $phone = "";
        $pickup = "";
        $dropoff = "";
        $date = "";
        $time = "";
        $budget = "";
        $status = "";
        $driver_id = "";
        $message = "";
        $distance = "";
        $errors = array(); 


        /* for demo and simulation of passenger code generation
        $fname = substr($_SESSION['firstname'],0,1);
        $lname = substr($_SESSION['lastname'],0,1);
        $pnumber = substr($_SESSION['phone'],-4);
        $passenger_code = $fname . $lname . $pnumber;
        */


        // SUBMIT BOOKING
        if (isset($_POST['submit'])) {
          // receive all input values from the form
          $passcode = mysqli_real_escape_string($link, $_POST['passcode']);
          $category = mysqli_real_escape_string($link, $_POST['category']);
          $bike_status = mysqli_real_escape_string($link, $_POST['bike_status']);
          $firstname = mysqli_real_escape_string($link, $_POST['firstname']);
          $lastname = mysqli_real_escape_string($link, $_POST['lastname']);
          $phone = mysqli_real_escape_string($link, $_POST['phone']);
          $pickup = mysqli_real_escape_string($link, $_POST['pickup']);
          $dropoff = mysqli_real_escape_string($link, $_POST['dropoff']);
          $date = mysqli_real_escape_string($link, $_POST['date']);
          $time = mysqli_real_escape_string($link, $_POST['time']);
          $budget = mysqli_real_escape_string($link, $_POST['budget']);
          $status = mysqli_real_escape_string($link, $_POST['status']);
          $driver_id = mysqli_real_escape_string($link, $_POST['driver_id']);
          $distance = mysqli_real_escape_string($link, $_POST['distance']);
          $message = mysqli_real_escape_string($link, $_POST['message']);

          // Submit booking if there are no errors in the form
          if (count($errors) == 0) {
            $query = "INSERT INTO bookings (
              passcode, category, bike_status, firstname, lastname, phone, pickup, dropoff, date, time, budget, message, status, driver_id, distance) 
                      VALUES('$passcode', '$category', '$bike_status', '$firstname', '$lastname', '$phone', '$pickup', '$dropoff', '$date', '$time', '$budget', '$message', '$status', '$driver_id', '$distance')";
            $retval = mysqli_query($link, $query);
          }
          if(! $retval ) {
               die('Could not enter data: ' . mysql_error());
            }
         
            $_SESSION['success'] = " 
                <div class='uk-alert-success uk-text-large uk-text-center uk-padding' uk-alert>
                    <a class='uk-alert-close' uk-close></a>
                    <p>Successfully submitted for <strong>$firstname $lastname </strong></p>
                </div> ";
          
            }

    if ( !session_id() ) {
        session_start();
    }
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-volunteers.php");
        exit;
    }

    include "includes/nav_volunteer.php" 
?>
<?php 
// for demo and simulation of passenger code generation
$fname = substr($firstname,0,1);
$lname = substr($lastname,0,1);
$pnumber = substr($phone,-4);

$passenger_code = $fname . $lname . $pnumber;
 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">

             <!-- main content -->
            <div class="uk-container-small uk-container-padded">
                <h3><?php echo $_SESSION['success']; ?></h3>
                <div class="uk-padding-small uk-text-center">
                    <a href="admin-rides_pending.php" class="uk-button uk-button-default">View Rides</a>
                    <a href="admin-add-ride.php" class="uk-button uk-button-primary">Add New Ride</a>
                </div>
                <p class="uk-text-center uk-margin-remove-bottom">Below is the generated passenger code for <?php echo $firstname; ?> <?php echo $lastname;; ?></p>
                <h3 class="uk-text-bold uk-margin-remove-top uk-text-center">Passenger Code: <?php echo $passcode; ?> </h3>
            </div>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>