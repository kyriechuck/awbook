
<?php
    session_start(); //session start

    //store posted values in session variables
    $_SESSION['category'] = $_SESSION['category'];
    $_SESSION['bike_status'] = $_POST['bike_status'];
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['phone'] = $_POST['phone'];
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['pickup'] = $_POST['pickup'];
    $_SESSION['dropoff'] = $_POST['dropoff'];
    $_SESSION['date'] = $_POST['date'];
    $date_old = $_SESSION["date"];
    $date = date("M, d, Y", strtotime($date_old));
    $_SESSION['time'] = $_POST['time'];
    $_SESSION['budget'] = $_POST['budget'];
    $_SESSION['driver_id'] = $_POST['driver_id'];
    $_SESSION['status'] = $_POST['status'];
    
 
    // create passenger code based on first name and last name
    $fname = substr($_POST['firstname'],0,1);
    $lname = substr($_POST['lastname'],0,1);
    $pnumber = substr($_POST['phone'],-4);
    $passenger_code = $fname . $lname . $pnumber;

    // store passenger code in session variable
    $_SESSION['passcode'] = $passenger_code; 
    
    
?> 
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>Book a Ride - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>



    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">A summary of your booking is shown below. Please review then confirm to book.</P>
            
            <div class="uk-container-padded">
                <form class=" uk-form " name="book-confirm" method="POST" action="book-submitted.php">
                    <div class="uk-grid">
                        <div class="uk-width-1-1 ">
                            <h3 class="uk-text-bold uk-text-purple">Your Details</h3>
                        </div>
                        <div class="uk-width-1-1">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-1-3"><p class=" ">Category</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_SESSION["category"]; ?></h4>
                                        </td>
                                    </tr>
                                    <?php

                                        if ($_POST['bike_status'] == 'Yes') {
                                            echo '<tr>
                                                    <td class="uk-width-1-3"><p class="">Can ride a bike?</p></td>
                                                    <td class="uk-width-2-3">
                                                        <h4 class="uk-text-bold">Yes, I can.</h4>
                                                    </td>
                                                </tr>';
                                        } else {
                                            echo '';
                                        }
                                    ?>
                                    <tr>
                                        <td class="uk-width-1-3"><p class=" ">Name</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["firstname"]; ?> <?php echo $_POST["lastname"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Contact Number</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["phone"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Email Address</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["email"]; ?> </h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="uk-width-1-1  uk-margin-large-top">
                            <h3 class="uk-text-bold uk-text-purple">Ride Details</h3>
                        </div>
                        <div class="uk-width-1-1">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Pick-up Point</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["pickup"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Drop-off Point</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["dropoff"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Pick-up Time</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $_POST["time"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-1-3"><p class="">Pick-up Date</p></td>
                                        <td class="uk-width-2-3">
                                            <h4 class="uk-text-bold"><?php echo $date; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class="">Budget</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class="uk-text-bold"><?php echo $_POST["budget"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class="">Assigned Driver</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class="uk-text-bold"><?php echo $_POST["driver_id"]; ?></h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="uk-width-1-1 uk-margin-top">
                            <h3 class="uk-text-bold uk-text-purple">Additional Instructions</h3>
                            <p class="uk-margin-bottom">Please enter any additional instructions or special request in the box below. E.g. Return Time, Need of vehicle going back to the hospital, etc.</p>
                        </div>
                        <div class="uk-width-1-1 ">
                            <textarea class="uk-textarea uk-text-bold" name="message" rows="10" placeholder="Enter your message"></textarea>
                        </div>
                    </div>

                    <div class="uk-margin uk-text-center">
                        <div class="uk-margin-bottom uk-margin-large-top">
                            <p class="uk-text-bold uk-text-purple"><input class="uk-checkbox" type="checkbox" style="border-radius:5px; margin-right: .5rem;" required> I confirm that the above information are correct and agree to the <a href="privacy.php" target="_blank">Policies and Disclaimers</a></p>
                        </div>
                    
                        <a href="javascript:history.go(-1)" class="uk-button">Back</a>
                        <input class="uk-button uk-button-primary" type="submit" value="Confirm and Submit Booking" name="submitconfirmed">
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>