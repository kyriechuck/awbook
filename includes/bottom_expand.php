<section class="uk-section uk-section-medium uk-section-purple">
    <div class="uk-container">
        <div class="uk-grid">
            <div class="uk-width-1-2@m uk-text-left uk-margin-large-bottom@s">
                <p class="uk-text-small uk-margin-remove">Office Address: <br>GF, UPSCALE Innovation Hub <br>
                National Engineering Centre, UP Diliman, Quezon City</p>
            </div>
            <div class="uk-width-1-2@m uk-text-right@m">
                <p class="uk-text-small uk-margin-remove uk-text-bold">Driving People Towards Equal Opportunities</p>
                <ul class="uk-nav uk-text-small">
                    <li class="uk-margin-right@m"><a href="https://www.facebook.com/accessiwheels/" uk-icon="icon: facebook">Follow Us on Facebook</a></li>
                    <li class="uk-margin-right@m"><a href="privacy.php">Privacy Policy</a></li>
                    <li class="uk-text-green"><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>