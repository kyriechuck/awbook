
<!-- start navigation -->
<div class="uk-navbar-container">
    <div class="uk-container">
        <nav class="uk-navbar">
            <div class="uk-navbar-left uk-text-bold">
                <a href="index-admin.php" class="uk-navbar-item uk-logo">
                    <img src="images/ui/AccessiWheels_Logo-Original.png" />
                </a>
                <ul class="uk-navbar-nav uk-visible@m">
                    <li><a href="index-admin.php"><span class=" uk-margin-small-right" uk-icon="icon: home"></span> Dashboard</a></li>
                    <li><a href="admin-rides_pending.php"><span class=" uk-margin-small-right" uk-icon="icon: server"></span>Rides</a></li>
                    <li hidden><a href="admin-bikes.php"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Bike Requests</a></li>
                    <li><a href="admin-drivers.php"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Drivers</a></li>
                    <li><a href="admin-volunteers.php"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Volunteers</a></li>
                    <li hidden><a href="signup-admin.php"><span class=" uk-margin-small-right" uk-icon="icon: file-text"></span>Admins</a></li>
                    <li hidden><a href="#"><span class=" uk-margin-small-right" uk-icon="icon: file-text"></span>Reports</a></li>
                </ul>
            </div>

            <div class="uk-navbar-right uk-text-bold">
                <ul class="uk-navbar-nav uk-visible@m">
                    <li hidden><a  href=""><span uk-icon="icon: cog"></span></a></li>
                    <li hidden><a  href=""><span uk-icon="icon: bell" alt="Notifications" value="Notifications"></span></a></li>
                    
                </ul>
                <p class="uk-visible@m uk-margin uk-text-purple uk-text-small">AW Admin <?php echo htmlspecialchars($_SESSION["username"]); ?>  <span class="uk-icon" uk-icon="user" style="margin-right: .5rem;"></span><span class="uk-icon" uk-icon="triangle-down"></span>
                </p>
                <div uk-dropdown class="uk-text-normal">
    				<ul class="uk-nav uk-dropdown-nav uk-visible@m uk-align-left uk-margin-remove-bottom">
                        <li hidden><a href=""><span class="uk-icon" uk-icon="user" style="margin-right: .5rem;"></span> admin@gmail.com</a></li>
                        <li ><a  href=""><span uk-icon="icon: cog" style="margin-right: .5rem;"></span> Settings</a></li>
                        <li class="uk-nav-divider"></li>
    					<li><a href="logout.php"><span uk-icon="icon: unlock" style="margin-right: .5rem;"></span>Logout</a></li>
    				</ul>
                </div>
            </div>
            <a href="#offcanvas" uk-toggle="" class="uk-text-white uk-navbar-toggle uk-hidden@m uk-icon uk-navbar-toggle-icon" uk-navbar-toggle-icon></a>
        </nav>
    </div>
</div>
<!-- end navigation -->

<!-- start offcanvas mobile menu -->
<div id="offcanvas" uk-offcanvas="flip:true;mode:push; overlay:true;">
    <div class="uk-offcanvas-bar">
    	<button class="uk-offcanvas-close" type="button" uk-close></button>
    	<a href="index.php" class="uk-navbar-item uk-logo">
            <p><img src="images/ui/AccessiWheels_Logo-ForDarkBG.png" /></p>
        </a>
        <p class="uk-text-bold uk-text-green uk-margin-remove-top">Driving People Towards Equal Opportunities</p>

        <ul class="uk-nav uk-nav-default">
            <li><a href="index-admin.php"><span class=" uk-margin-small-right" uk-icon="icon: home"></span> Dashboard</a></li>
            <li><a href="admin-rides_pending.php"><span class=" uk-margin-small-right" uk-icon="icon: server"></span>Rides</a></li>
            <li><a href="admin-drivers.php"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Drivers</a></li>
             <li><a href="admin-volunteers.php"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Volunteers</a></li>
            <li hidden><a href="#"><span class=" uk-margin-small-right" uk-icon="icon: users"></span>Users</a></li>
            <li hidden><a href="#"><span class=" uk-margin-small-right" uk-icon="icon: file-text"></span>Reports</a></li>
        </ul>
        <hr>
        <ul class="uk-nav uk-nav-default uk-margin-medium-top">
            <li class="uk-text-bold"><a href=""><?php echo $_SESSION['username']; ?></a></li>
            <li hidden><a href="">rider@gmail.com</a></li>
            <li class="uk-nav-divider"></li>
            <li hidden><a href="">Account</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
    </div>
</div>
<!-- start offcanvas mobile menu -->