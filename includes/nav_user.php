
<!-- start navigation -->
<div class="uk-navbar-container">
    <div class="uk-container">
        <nav class="uk-navbar">
            <div class="uk-navbar-left uk-text-bold">
                <a href="index.php" class="uk-navbar-item uk-logo">
                    <img src="images/ui/AccessiWheels_Logo-Original.png" />
                </a>
                <ul class="uk-navbar-nav">
                    <li><a href="book.php"><span class="uk-icon" uk-icon="sign-in" style="margin-right: .5rem;"></span>Book a Ride</a></li>
                    <li><a href="ride-status.php"><span class="uk-icon" uk-icon="check" style="margin-right: .5rem;"></span>Check Ride Status </a></li>
                    <li><a href="my-rides.php"><span class="uk-icon" uk-icon="clock" style="margin-right: .5rem;"></span>Ride History</a></li>
                    <li hidden class="uk-hidden@m"><a href="login.php"><span class="uk-icon" uk-icon="sign-in" style="margin-right: .5rem;">Login</a></li>
                </ul>
            </div>

            <div class="uk-navbar-right uk-text-bold">
                <ul class="uk-navbar-nav uk-visible@m">
                    <li hidden><a href="login.php"><span class="uk-icon" uk-icon="sign-in" style="margin-right: .5rem;">Login</a></li>
                </ul>
            </div>
            <a href="#offcanvas" uk-toggle="" class="uk-text-white uk-navbar-toggle uk-hidden@m uk-icon uk-navbar-toggle-icon"></a>
        </nav>
    </div>
</div>
<!-- end navigation -->

<!-- start offcanvas mobile menu -->
<div id="offcanvas" uk-offcanvas="flip:true;mode:push; overlay:true;">
    <div class="uk-offcanvas-bar">
    	<button class="uk-offcanvas-close" type="button" uk-close></button>
    	<a href="index.php" class="uk-navbar-item uk-logo">
            <p><img src="images/ui/AccessiWheels_Logo-ForDarkBG.png" /></p>
        </a>
        <p class="uk-text-bold uk-text-green uk-margin-remove-top">Driving People Towards Equal Opportunities</p>

        <ul class="uk-nav uk-nav-default">
            <li class="uk-text"><a href="book.php">Book a Ride</a></li>
            <li class="uk-text"><a href="ride-status.php">Ride Status</a></li>
        </ul>
        <hr>
        <ul class="uk-nav uk-nav-default uk-margin-medium-top">
            <li class="uk-text-bold uk-text-small"><a href="login.php"><span class="uk-icon" uk-icon="sign-in" style="margin-right: .5rem;">Login</a></li>
        </ul>
    </div>
</div>
<!-- start offcanvas mobile menu -->
