
<!-- start navigation -->
<div class="uk-navbar-container">
    <div class="uk-container">
        <nav class="uk-navbar">
            <div class="uk-navbar-left uk-text-bold">
                <a href="index-driver.php" class="uk-navbar-item uk-logo">
                    <img src="images/ui/AccessiWheels_Logo-Original.png" />
                </a>
                <ul class="uk-navbar-nav uk-visible@m">
                    <li><a href="mytrips.php">My Trips</a></li>
                </ul>
            </div>

            <!-- 
            <div class="uk-navbar-right uk-text-bold">
                <ul class="uk-navbar-nav uk-visible@m">
                    <li><a  href=""><span uk-icon="icon: bell"></span></a></li>
                </ul>
                <p class="uk-visible@m uk-margin uk-text-purple uk-text-small">Driver Alexander Bell  <span class="uk-icon" uk-icon="user" style="margin-right: .5rem;"></span><span class="uk-icon" uk-icon="triangle-down"></span>
                </p>
                <div uk-dropdown class="uk-text-normal">
    				<ul class="uk-nav uk-dropdown-nav uk-visible@m uk-align-left uk-margin-remove-bottom">
                        <li class="uk-text-bold"><a href="">Driver Code: CB88376</a></li>
    					<li><a href="">driver@gmail.com</a></li>
                        <li class="uk-nav-divider"></li>
                        <li><a href="">Account</a></li>
    					<li><a href="logout.php">Logout</a></li>
    				</ul>
                </div>
            </div>
            <a href="#offcanvas" uk-toggle="" class="uk-text-white uk-navbar-toggle uk-hidden@m uk-icon uk-navbar-toggle-icon" uk-navbar-toggle-icon></a>
            -->
        </nav>
    </div>
</div>
<!-- end navigation -->

<!-- start offcanvas mobile menu -->
<div id="offcanvas" uk-offcanvas="flip:true;mode:push; overlay:true;">
    <div class="uk-offcanvas-bar">
    	<button class="uk-offcanvas-close" type="button" uk-close></button>
    	<a href="index.php" class="uk-navbar-item uk-logo">
            <p><img src="images/ui/AccessiWheels_Logo-ForDarkBG.png" /></p>
        </a>
        <p class="uk-text-bold uk-text-green uk-margin-remove-top">Driving People Towards Equal Opportunities</p>

        <ul class="uk-nav uk-nav-default">
            <li class="uk-text"><a href="book.php">Book a Ride</a></li>
            <li class="uk-text"><a href="ride-status.php">Ride Status</a></li>
            <li><a href="my-rides-pending.php">My Rides</a></li>
        </ul>
        <hr>
        <ul class="uk-nav uk-nav-default uk-margin-medium-top">
            <li class="uk-text-bold"><a href="">Passenger Code: CB88376</a></li>
            <li><a href="">rider@gmail.com</a></li>
            <li class="uk-nav-divider"></li>
            <li><a href="">Account</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
    </div>
</div>
<!-- start offcanvas mobile menu -->