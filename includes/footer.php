<!-- start footer area -->
<section class="uk-footer uk-padding">
    <div class="uk-container">
        <div class="uk-grid">
            <div class="uk-width-1-2@m uk-text-left uk-text-small">
                <p>AccessiWheels. All Rights Reserved. Copyright 2019-2020</p>
            </div>
            <div class="uk-width-1-2@m uk-text-right@m">
                <p><a href="http://lilypaddigital.co"><img src="https://lilypaddigital.co/mediabox/images/UI/powered-by-lilypad.png" title="Powered by Lily Pad Digital Solutions" width="190" alt="Powered by Lily Pad Digital Solutions"/></a></p>
            </div>
        </div>
    </div>
</section>

<!-- end footer area -->