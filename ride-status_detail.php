<?php
    session_start(); //session start
   
    // Include config file
    include "config.php";

    $name = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
    $pickup = $_SESSION['pickup'];
    $dropoff = $_SESSION['dropoff'];
    $date = $_SESSION['date'];
    $time = $_SESSION['time'];
    $message = $_SESSION['message'];
    $budget = $_SESSION['budget'];

?>
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        
        <title>Ride Details - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Ride Status</h1>
            <P class="uk-text uk-text-lead uk-margin-remove-top">Get real-time status of your ride.</P>

            <!-- main content -->
            
                <?php 
                    $query = $_POST['query']; 
                    // gets value sent over search form
                    // Attempt select query execution
                    $sql = "SELECT bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname, drivers.phone as driverPhone
                        FROM drivers
                        JOIN bookings on drivers.id = bookings.driver_id 
                        WHERE bookings.passcode like '%".$query."%' AND NOT bookings.status='completed' AND NOT bookings.status='NAD' ORDER by date ASC LIMIT 2 ";

                    if($result = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result) > 0){
                            
                            while($row = mysqli_fetch_array($result)){
                                $id = $row["id"];
                                $passengercode = $row['passcode'];
                                $category = $row["category"];
                                $name = $row["firstname"] ." ". $row['lastname'];
                                $dropoff = $row["dropoff"];
                                $pickup = $row["pickup"];
                                $date_old = $row["date"];
                                $date = date("M, d, Y", strtotime($date_old));
                                $time = $row["time"];
                                $status = $row["status"];
                                $message = $row["message"];
                                $budget = $row["budget"];
                                $driver_id = $row["driver_id"];
                                $driverFirstname = $row["driverFirstname"];
                                $driverLastname = $row["driverLastname"];
                                $driverPhone = $row["driverPhone"];
                                $date_added = $row["created_at"];

                                echo '
                                <div class="uk-container-small uk-container-padded uk-margin-bottom">
                                    <div class="uk-grid">
                                        <div class="uk-width-3-5@m">
                                            <h3 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">'. 'Name: '. $name.'</h3>
                                            <p class="uk-text uk-margin-remove-top">'. 'Passenger Code: '. $passengercode.'</p>
                                        </div>
                                        <div class="uk-width-1-5@m uk-visible@s">
                                            <p class="uk-margin-remove-bottom uk-text-small">ASSIGNED DRIVER</p>
                                            <h4 class="uk-text-bold uk-margin-remove-top">'. $driverFirstname.' '. $driverLastname. '<br>' .$driverPhone . '</h4> 
                                        </div>
                                        <div class="uk-width-1-5@m uk-visible@s">
                                            <p class="uk-margin-remove-bottom uk-text-small">STATUS</p>
                                            <h4 class="uk-text-bold uk-margin-remove-top uk-text">'. $status.'</h4>
                                        </div>
                                    </div>
                                    <div class="uk-grid uk-hidden@s">
                                        <div class="uk-width-1-2@m">
                                            <p class="uk-margin-remove-bottom uk-text-small uk-margin-top">ASSIGNED DRIVER</p>
                                            <h4 class="uk-text-bold uk-margin-remove-top">'. $driverFirstname .'</h4>
                                        </div>
                                        <div class="uk-width-1-2@m">
                                            <p class="uk-margin-remove-bottom uk-text-small uk-margin-top">STATUS</p>
                                            <h4 class="uk-text-bold uk-margin-remove-top uk-text">'. $status.'</h4>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="uk-margin uk-width-auto">
                                        <div class="uk-grid">
                                            <div class="uk-width-1-2@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">PICK-UP POINT</p>
                                                <h4 class="uk-margin-small uk-text-purple">'. $pickup.'</h4>
                                            </div>
                                            <div class="uk-width-1-2@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">DROP-OFF POINT</p>
                                                <h4 class="uk-margin-small uk-text-purple">'. $dropoff.'</h4>
                                            </div>
                                            
                                        </div>
                                        <hr>
                                        <div class="uk-grid ">
                                            <div class="uk-width-1-3@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">PICK-UP TIME</p>
                                                <h4 class="uk-margin-small ">'. $time.'</h4>
                                            </div>
                                            <div class="uk-width-1-3@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">PICK-UP DATE</p>
                                                <h4 class="uk-margin-small">'. $date.'</h4>
                                            </div>
                                            <div class="uk-width-1-3@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">BUDGET</p>
                                                <h4 class="uk-margin-small ">'. $budget.'</h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="uk-grid">
                                            <div class="uk-width-1-1@m">
                                                <p class=" uk-margin-remove-bottom uk-text-small uk-text">ADDITIONAL MESSAGE / INSTRUCTION</p>
                                                <p class="uk-margin-small">'. $message.'</p>
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                ';
                            }
                            
                            echo "</table>";
                            // Free result set
                            mysqli_free_result($result);
                        } else{
                            echo "No records matching your query were found.";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                    }
                    
                    // Close connection
                    mysqli_close($link);

                ?>

           
            <!-- end: main content -->
            
        </div>
    </div>
    <!-- end: main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>