
<?php
    session_start(); //session start

    //store posted values in session variables
    $_SESSION['category'] = $_POST['category'];
    
?>

<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>Book a Ride - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>


    <!-- start main section -->
    <div class="uk-section uk-section-lightbackground">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Book A Ride</h1>
            <P class="uk-text uk-text uk-margin-remove-top">All fields are <span class="uk-text-danger uk-text-bold">required.</span></P>
            
            <div class="uk-grid ">    
                <div class="uk-width-2-3@m" >
                    <div class="uk-container ">
                        <form class="uk-form uk-container-padded" name="book-pudo" action="book-confirm.php" method="POST">
                            <fieldset class="uk-fieldset">
                                <!-- simulation: display selection for frontliners -->
                                <?php
                                    $category = $_POST['category'];

                                    if ($category == 'Frontliner') {
                                        echo '<div class="uk-margin uk-grid-small uk-width-auto uk-margin-remove-bottom">
                                        <h3 class="uk-text-bold">Magandang Araw, Frontliner!</h3>
                                            <label class="uk-text-bold"><input class="uk-checkbox" type="radio" name="bike_status" value="Yes" <?=($bikeavailability == "yes" ? "checked": "") ?> Yes, I can.</label>
                                            <label class="uk-text-bold"><input class="uk-checkbox" type="radio" name="bike_status" value="No" <?=($bikeavailability == "no" ? "checked": "") ?> Nope</label>
                                        </div>
                                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Can you ride a bike? We might be able to help you use one.</p>
                                        <hr>';
                                    } else {
                                        echo '<div class="uk-margin uk-grid-small uk-width-auto uk-margin-remove-bottom">
                                            <h3 class="uk-text-bold">Magandang Araw!</h3>
                                        </div>
                                        <hr>';
                                    }
                                ?>
                                
                            </fieldset>
                            
                            <fieldset class="uk-fieldset">
                                <div class="uk-grid uk-margin">
                                    <div class="uk-width-1-2@m uk-margin-top uk-margin-remove-top@s" >
                                        <label class="">First Name</label>
                                        <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder=""  required>
                                        <p hidden class="uk-text uk-margin-remove-top uk-text-muted">Enter your first name</p>
                                    </div>
                                    <div class="uk-width-1-2@m uk-margin-top" >
                                        <label class="">Last Name</label>
                                        <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="" required>
                                         <p hidden class="uk-text uk-margin-remove-top uk-text-muted">Enter your last name</p>
                                    </div>
                                </div>
                                <div class="uk-margin uk-grid">
                                    <div class="uk-width-2-5@m uk-margin-top uk-margin-remove-top@s" >
                                        <label class="">Contact Number</label>
                                        <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="" required>
                                        <p class="uk-text uk-margin-remove-top uk-text-muted">Enter your mobile number.</p>
                                    </div>
                                    <div class="uk-width-3-5@m uk-margin-top" >
                                        <label class="">Email Address</label>
                                        <input name="email" class="uk-input uk-text-bold" type="email" placeholder="" required>
                                        <p class="uk-text uk-margin-remove-top uk-text-muted">Enter a valid email address.</p>
                                    </div>
                                    <p class="uk-text uk-margin-remove-top uk-text-muted">We will update you via your contact number and email address.</p>
                                </div>
                                <hr>
                                <div class="uk-margin uk-width-auto">
                                    <label class="">Pick-up Point</label>
                                    <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="" required>
                                    <p class="uk-text uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <label class="">Drop-off Point</label>
                                    <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="" required>
                                    <p class="uk-text uk-margin-remove-top uk-text-muted">Please provide complete street address, hotel / hospital / airport name, and city.</p>
                                </div>
                                <hr>

                                <div class="uk-grid uk-margin">
                                    <div class="uk-width-1-2@m uk-margin-top uk-margin-remove-top@s" >
                                        <label class="">Pick-up Date</label>
                                        <input name="date" class="uk-input" type="date" placeholder="" required>
                                        <p hidden class="uk-text uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                                    </div>
                                    <div class="uk-width-1-2@m uk-margin-top" >
                                        <label class="">Pick-up Time</label>
                                        <select name="time" class="uk-select" required>
                                            <option>05:00 AM</option>
                                            <option>05:30 AM</option>
                                            <option>06:00 AM</option>
                                            <option>06:30 AM</option>
                                            <option>07:00 AM</option>
                                            <option>07:30 AM</option>
                                            <option>08:00 AM</option>
                                            <option>08:30 AM</option>
                                            <option>09:00 AM</option>
                                            <option>09:30 AM</option>
                                            <option>10:00 AM</option>
                                            <option>10:30 AM</option>
                                            <option>11:00 AM</option>
                                            <option>11:30 AM</option>
                                            <option>12:00 PM</option>
                                            <option>12:30 PM</option>
                                            <option>01:00 PM</option>
                                            <option>01:30 PM</option>
                                            <option>02:00 PM</option>
                                            <option>02:30 PM</option>
                                            <option>03:00 PM</option>
                                            <option>03:30 PM</option>
                                            <option>04:00 PM</option>
                                            <option>04:30 PM</option>
                                            <option>05:00 PM</option>
                                            <option>05:30 PM</option>
                                            <option>06:00 PM</option>
                                            <option>06:30 PM</option>
                                            <option>07:00 PM</option>
                                            <option>07:30 PM</option>
                                            <option>08:00 PM</option>
                                            <option>08:30 PM</option>
                                            <option>09:00 PM</option>
                                            <option>09:30 PM</option>
                                            <option>10:00 PM</option>
                                            <option>10:30 PM</option>
                                            <option>11:00 PM</option>
                                            <option>11:30 PM</option>
                                            <option>12:00 AM</option>
                                            
                                        </select>
                                        <p hidden class="uk-text uk-margin-remove-top uk-text-muted">Select Time</p>
                                    </div>
                                </div>
                                <div class="uk-margin uk-width-auto">
                                    <label class="">Budget</label>
                                    <input name="budget" class="uk-input uk-text-bold" type="number" placeholder="0.00" required>
                                    <p class="uk-text uk-margin-remove-top uk-text-muted">Please enter your budget for this trip.</p>
                                </div>
                                <div class="uk-margin uk-width-auto" hidden>
                                    <select name="status" class="uk-select" value="pending">
                                        <option value="pending">Pending</option>
                                        <option value="received">Received</option>
                                        <option value="assigned-driver">Assigned Driver</option>
                                        <option value="scheduled">Scheduled</option>
                                    </select>
                                </div>
                                <input type="hidden" name="driver_id" value="0" class="uk-input uk-text-bold" type="text">

                                <input class="uk-button uk-button-primary uk-width-1-1" type="submit" value="Review Booking" name="submit">
                            </fieldset>
                                
                            
                        </form>
                    </div>
                </div>

                <div class="uk-width-1-3@m">
                </div>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>