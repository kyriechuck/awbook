<?php
session_start(); //session start

// Include config file
require_once "config.php";

// Check existence of id parameter before processing further
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    // Get URL parameter
    $id =  trim($_GET["id"]);
    
    // Prepare a select statement
    $sql = "SELECT * FROM volunteers WHERE id = ?";
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);

            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $id = $row["id"];
                $username = $row['username'];
                $password = $row['password'];
                $email = $row['email'];
                $firstname = $row["firstname"];
                $lastname = $row['lastname'];
                $name = $firstname .' '. $lastname;
                $phone = $row["phone"];
                $address = $row["address"];
                $facebook = $row['facebook'];
                $created_atold = $row["created_at"];
                $created_at = date("M, d, Y", strtotime($created_atold));
            } else{
                // URL doesn't contain valid id. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
    
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
}  else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>


    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom"><?= $name ?></h1>
            <P class="uk-text uk-text uk-margin-remove-top">Volunteer Summary View</P>
            <p class="uk-text uk-text-bold uk-margin-remove-top">Date Added: <?php echo $created_at; ?></p>
            
            <div class="uk-container-padded">
                <form class="uk-form uk-container-padded" name="signup" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                    <h3 class="uk-text-bold">Account Details</h3>
                    <fieldset class="uk-fieldset">
                        <div class="uk-margin">
                            <input name="username" class="uk-input uk-text-bold" type="text" placeholder="Username" value="<?php echo $username; ?>" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter username</p>
                            <p class=" uk-alert-danger"><?php echo $username_err; ?></p>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="password" class="uk-input uk-text-bold" type="password" placeholder="Password" value="<?php echo $password; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter unique password</p>
                                <p class=" uk-alert-danger"><?php echo $password_err; ?></p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="email" class="uk-input uk-text-bold" type="text" placeholder="Email Address" value="<?php echo $email; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter a valid email address</p>
                            </div>
                        </div>

                        <h3 class="uk-text-bold">Personal Details</h3>
                        <p>Please fill out all fields.</p>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="firstname" class="uk-input uk-text-bold" value="<?php echo $firstname; ?>" type="text" placeholder="First Name" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="lastname" class="uk-input uk-text-bold" type="text" value="<?php echo $lastname; ?>" placeholder="Last Name" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                            </div>
                        </div>

                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="phone" class="uk-input uk-text-bold" type="text" value="<?php echo $phone; ?>" placeholder="Contact Number" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter contact number</p>
                            </div>
                            <div class="uk-width-2-3@m" >
                                <input name="facebook" class="uk-input uk-text-bold" type="text" value="<?php echo $facebook; ?>" placeholder="Facebook URL" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter facebook URL</p>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <input name="address" class="uk-input uk-text-bold" type="text" value="<?php echo $address; ?>" placeholder="Enter location" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Location</p>
                        </div>

                        <?php echo "<a class='uk-button uk-button-primary uk-margin-top uk-width-1-1' href='admin-volunteer-update.php?id=".$row['id']."' title='Edit Volunteer Details' data-toggle='tooltip'>Edit Details <span class='uk-icon' uk-icon='file-edit'></span></a>"; ?>

                    </fieldset>
                        
                    
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>