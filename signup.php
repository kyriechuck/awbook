
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}
?>
    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Sign Up</h1>
            <P class="uk-text uk-text uk-margin-remove-top">Please fill this form to create an account. All fields are required.</P>
            
            <div class="uk-container-padded">
            <form class="uk-form uk-container-padded" name="signup" action="signup-confirm.php" method="POST">
                <h3 class="uk-text-bold">Account Details</h3>
                <fieldset class="uk-fieldset">
                    <div class="uk-margin">
                        <input name="username" class="uk-input uk-text-bold" type="text" placeholder="Username" required>
                        <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter username</p>
                    </div>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="password" class="uk-input uk-text-bold" type="password" placeholder="Password" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter unique password</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="email" class="uk-input uk-text-bold" type="text" placeholder="Email Address" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter a valid email address</p>
                        </div>
                    </div>
                    <hr>
                </fieldset>

                <fieldset class="uk-fieldset">
                    <h3 class="uk-text-bold">Personal Details</h3>
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-1-2@m" >
                            <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                        </div>
                        <div class="uk-width-1-2@m" >
                            <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" required>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                        </div>
                    </div>
                
                    <div class="uk-grid uk-margin">
                        <div class="uk-width-2-3@m" >
                            <input name="address" class="uk-input uk-text-bold" type="text" placeholder="Address">
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please provide complete address.</p>
                        </div>
                        <div class="uk-width-1-3@m" >
                            <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number">
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter phone number.</p>
                        </div>
                    </div>
                    <input class="uk-button uk-button-primary uk-width-1-1 uk-margin-top" type="submit" value="Submit" name="submit">
                </fieldset>
                    
                
            </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>