<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">My Rides</h1>
                    <p>Browse all your ride activity and ride details here.</p>
                </div>
                <div class="uk-width-1-2@m">
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li ><a href="my-rides-pending.php">Pending Rides</a></li>
                        <li ><a href="my-rides-scheduled.php">Scheduled Rides</a></li>
                        <li class="uk-active"><a href="my-rides-finished.php">Finished Rides</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-3@m ">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <form class="uk-search uk-search-default uk-align-right">
                                <span uk-search-icon></span>
                                <input class="uk-search-input uk-text-small " type="search" placeholder="Search Rides">
                            </form>
                        </div>
                        <div class="uk-width-1-2@m">
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <!-- switcher content -->
            <div class="uk-container-small uk-container-padded">
                <div class="uk-overflow-auto">
                    <table class="uk-table uk-table-responsive uk-table-middle uk-table-divider">
                        <thead>
                            <tr>
                                <th class="uk-table-shrink"></th>
                                <th class="uk-width-medium">Ride</th>
                                <th class="uk-width-small">From</th>
                                <th class="uk-width-small">Date</th>
                                <th class="uk-width-small">Budget</th>
                                <th class="uk-table-shrink">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input class="uk-checkbox" type="checkbox"></td>
                                <td class="uk-table-link">
                                    <a class="uk-link-reset uk-margin-remove-bottom" href="my-rides_details-finished.php">
                                        <p class="uk-margin-remove-bottom uk-text-bold">Ride to Diamond Residences</p>
                                        <p class="uk-margin-remove-top">SEDAN</p>
                                    </a>
                                    
                                </td>
                                <td class="uk-text-wrap">Malalahanin, Teachers Village, Quezon City</td>
                                <td class="uk-text-wrap">
                                    <date>Apr 10, 2020</date>
                                </td>
                                <td class="uk-text-wrap">Php 350</td>
                                <td class="uk-text-wrap uk-text-bold">Finished</td>
                            </tr>
                            <tr>
                                <td><input class="uk-checkbox" type="checkbox"></td>
                                <td class="uk-table-link">
                                    <a class="uk-link-reset uk-margin-remove-bottom" href="my-rides_details-finished.php">
                                        <p class="uk-margin-remove-bottom uk-text-bold">Ride to Ateneo De Manila</p>
                                        <p class="uk-margin-remove-top">SEDAN</p>
                                    </a>
                                    
                                </td>
                                <td class="uk-text-wrap">Malalahanin, Teachers Village, Quezon City</td>
                                <td class="uk-text-wrap">
                                    <date>Apr 10, 2020</date>
                                </td>
                                <td class="uk-text-wrap">Php 350</td>
                                <td class="uk-text-wrap uk-text-bold uk-text-danger">Cancelled</td>
                            </tr>
                            <tr>
                                <td><input class="uk-checkbox" type="checkbox"></td>
                                <td class="uk-table-link">
                                    <a class="uk-link-reset uk-margin-remove-bottom" href="my-rides_details.php">
                                        <p class="uk-margin-remove-bottom uk-text-bold">Ride to UP Diliman</p>
                                        <p class="uk-margin-remove-top">SEDAN</p>
                                    </a>
                                    
                                </td>
                                <td class="uk-text-wrap">Malalahanin, Teachers Village, Quezon City</td>
                                <td class="uk-text-wrap">
                                    <date>Apr 10, 2020</date>
                                </td>
                                <td class="uk-text-wrap">Php 350</td>
                                <td class="uk-text-wrap uk-text-bold uk-text-danger">Sorry, Driver Cancelled</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">My Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>