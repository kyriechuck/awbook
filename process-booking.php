<?php

  session_start(); //session start

  // Include config file
include "config.php";

$_SESSION['message'] = $_POST['message'];
  
// initializing variables
$passcode = "";
$category = "";
$bike_status = "";
$firstname = ""; 
$lastname = "";
$phone = "";
$email = "";
$pickup = "";
$dropoff = "";
$date = "";
$time = "";
$budget = "";
$status = "";
$driver_id = "";
$message = "";
$distance = "";
$errors = array(); 


/* for demo and simulation of passenger code generation
$fname = substr($_SESSION['firstname'],0,1);
$lname = substr($_SESSION['lastname'],0,1);
$pnumber = substr($_SESSION['phone'],-4);
$passenger_code = $fname . $lname . $pnumber;
*/


// SUBMIT BOOKING
if (isset($_POST['submitconfirmed'])) {
  // receive all input values from the form
  $passcode = mysqli_real_escape_string($link, $_SESSION['passcode']);
  $category = mysqli_real_escape_string($link, $_SESSION['category']);
  $bike_status = mysqli_real_escape_string($link, $_SESSION['bike_status']);
  $firstname = mysqli_real_escape_string($link, $_SESSION['firstname']);
  $lastname = mysqli_real_escape_string($link, $_SESSION['lastname']);
  $phone = mysqli_real_escape_string($link, $_SESSION['phone']);
  $email = mysqli_real_escape_string($link, $_SESSION['email']);
  $pickup = mysqli_real_escape_string($link, $_SESSION['pickup']);
  $dropoff = mysqli_real_escape_string($link, $_SESSION['dropoff']);
  $date = mysqli_real_escape_string($link, $_SESSION['date']);
  $time = mysqli_real_escape_string($link, $_SESSION['time']);
  $budget = mysqli_real_escape_string($link, $_SESSION['budget']);
  $status = mysqli_real_escape_string($link, $_SESSION['status']);
  $driver_id = mysqli_real_escape_string($link, $_SESSION['driver_id']);
  $distance = mysqli_real_escape_string($link, $_SESSION['distance']);
  $message = mysqli_real_escape_string($link, $_SESSION['message']);

  // Submit booking if there are no errors in the form
  if (count($errors) == 0) {
  	$query = "INSERT INTO bookings (passcode, category, bike_status, firstname, lastname, phone, email, pickup, dropoff, date, time, budget, message, status, driver_id, distance) 
  			  VALUES('$passcode', '$category', '$bike_status', '$firstname', '$lastname', '$phone', '$email', '$pickup', '$dropoff', '$date', '$time', '$budget', '$message', '$status', '$driver_id', '$distance')";
  	mysqli_query($link, $query);
  }
  
}


