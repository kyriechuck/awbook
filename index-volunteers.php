<?php
// Initialize the session
session_start();

// Include config file
require_once "config.php";

$pending = mysqli_num_rows(mysqli_query($link, "SELECT * FROM bookings where status='pending'"));
$received = mysqli_num_rows(mysqli_query($link, "SELECT * FROM bookings where status='received'"));
$scheduled = mysqli_num_rows(mysqli_query($link, "SELECT * FROM bookings where status='scheduled'"));
$completed = mysqli_num_rows(mysqli_query($link, "SELECT * FROM bookings where status='completed'"));
$drivers_active = mysqli_num_rows(mysqli_query($link, "SELECT * FROM drivers where status='active' "));
$drivers_inactive = mysqli_num_rows(mysqli_query($link, "SELECT * FROM drivers where status='inactive'"));

$result = mysqli_query($link, "SELECT SUM(distance) as distance_sum FROM bookings where status='completed'");
$row = mysqli_fetch_assoc($result);
$total_distance = number_format($row['distance_sum'], 2);

     


?>
<!doctype html>
<html>
    <head>
        <title>Admin - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-volunteers.php");
        exit;
    }

    include "includes/nav_volunteer.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            <h1 class="uk-text-bold uk-text-green uk-margin-remove-bottom">Good day, volunteer!</h1>

            <div class="uk-margin uk-container-padded">
                <div class="uk-margin ">
                    <div class="uk-alert-success uk-animation-fade uk-text-center uk-padding" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p class="uk-text-bold uk-text-large">There are <span><?php echo $pending; ?> pending ride(s) waiting to be received. </span><a class="uk-button uk-button-primary" href="volunteer_pending.php">View Pending Rides</a></p>
                    </div>
                </div>
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@m">
                        <img src="images/aw-img.png">
                    </div>
                    <div class="uk-width-expand@m">
                        <h3 class="uk-text-bold uk-text-purple">Ride Performance</h3>
                        <div class="uk-margin uk-grid">
                            <div class="uk-width-1-3@m">
                                <p class="uk-text-bold uk-margin-remove-bottom">Received Rides</p>
                                <p class="uk-text-small uk-margin-remove-top">Received but still looking for drivers</p>
                                <p  class="uk-text-bold uk-text-large uk-margin-remove-top"><a href="volunteer_pending.php"><?php echo $received; ?></a></p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <p class="uk-text-bold uk-margin-remove-bottom">Scheduled Rides</p>
                                <p class="uk-text-small uk-margin-remove-top">Rides with assigned drivers</p>
                                <p class="uk-text-bold uk-text-large uk-margin-remove-top"><a href="admin-volunteer_scheduled.php"><?php echo $scheduled; ?></a></p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <p class="uk-text-bold uk-margin-remove-bottom">Completed Rides</p>
                                <p class="uk-text-small uk-margin-remove-top">Rides fulfilled. Phew. :)</p>
                                <p  class="uk-text-bold uk-text-large uk-margin-remove-top"><a href="volunteer_completed.php"><?php echo $completed; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-margin">
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-3@m">
                        <div class="uk-card uk-card-default uk-card-body">
                            <h4 class="uk-text-bold">Recent Feedbacks</h4>
                            <div class="uk-grid">
                                <div class="uk-width-2-5@m">
                                    <img src="images/avatar.jpg" height="70"  width="70"/>
                                </div>
                                <div class="uk-width-3-5@m uk-padding-remove-left">
                                    <p class="uk-text-bold">Juan Dela Cruz</p>
                                    <p class="uk-text-small">Ang dali gamitin. On time lagi dumating ang drivers and very well trained..</p>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-2-5@m">
                                    <img src="https://d1si3tbndbzwz9.cloudfront.net/basketball/player/2109/headshot.png" height="70"  width="70"/>
                                </div>
                                <div class="uk-width-3-5@m uk-padding-remove-left">
                                    <p class="uk-text-bold">Jeremy Lin</p>
                                    <p class="uk-text-small">This is the best platform i have tried so far..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-3@m">
                        <div class="uk-card uk-card-default uk-card-body">
                            <h3 class="uk-text-bold uk-text-purple">Trip Records</h3>
                            <div class="">
                                <p class="uk-text uk-margin-remove-bottom"> Total Distance Driven</p>
                                <p class="uk-text-large uk-text-green uk-text-bold uk-margin-remove-top"> <?php echo $total_distance; ?> km</p>
                            </div>
                            <div class="">
                                <p class="uk-text uk-margin-remove-bottom"> Total Partner Drivers</p>
                                <div class=" uk-child-width-expand@s uk-text-large uk-text-bold uk-margin-remove-top" uk-grid> 
                                    <div>
                                        <div class="uk-text-green"><span class="uk-icon" uk-icon="icon: users; ratio: 1.2" ></span> <?php echo $drivers_active; ?> Active</div>
                                    </div>
                                    <div>
                                        <div class="uk-text-danger"><span class="uk-icon" uk-icon="icon: users; ratio: 1.2" ></span> <?php echo $drivers_inactive; ?> Inactive</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-3@m" >
                        
                        <div class="uk-card uk-card-default uk-card-body uk-margin-small-top">
                            <h4 class="uk-text-bold">Revenue</h4>
                            <div class="">
                                <p class="uk-text">Insert Revenue chart here</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end main content -->

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>