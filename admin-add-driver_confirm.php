
<?php
    session_start(); //session start

    //store posted values in session variables
    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['lastname'] = $_POST['lastname'];
    $_SESSION['phone'] = $_POST['phone'];
    $_SESSION['address'] = $_POST['address'];
    $_SESSION['cartype'] = $_POST['cartype'];
    $_SESSION['carmodel'] = $_POST['carmodel'];
    $_SESSION['status'] = $_POST['status'];
    $_SESSION['facebook'] = $_POST['facebook'];
    $_SESSION['availability'] = $_POST['availability'];
    
?> 


<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>
 
<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>
    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Add New Driver</h1>
            <P class="uk-text uk-text uk-margin-remove-top">A summary is shown below. To make changes, click on the Edit Details button.</P>
            
            <div class="uk-container-padded">
                <form class=" uk-form " name="book-confirm" method="POST" action="admin-add-driver_submitted.php">
                    <div class="uk-grid">
                        
                        <div class="uk-width-1-1 form-title uk-margin">
                            <h3 class="uk-text-bold uk-text-purple">Personal Details</h3>
                        </div>
                        <div class="uk-width-1-1 form-step">
                            <table class="uk-table uk-table-divider">
                                <tbody>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Name</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["firstname"]; ?> <?php echo $_POST["lastname"]; ?></h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Address</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["address"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Facebook URL</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["facebook"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Availability</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["availability"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Contact Number</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["phone"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Car Type</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["cartype"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Car Model</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["carmodel"]; ?> </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="uk-width-small"><p class=" uk-text-bold">Status</p></td>
                                        <td class="uk-width-medium">
                                            <h4 class=""><?php echo $_POST["status"]; ?> </h4>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="uk-margin uk-text-center uk-margin-large-top">
                        <a href="book-pudo.php" class="uk-button">Edit Details</a>
                        <input class="uk-button uk-button-primary" type="submit" value="Confirm and Add Driver" name="submit-confirmed">
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>