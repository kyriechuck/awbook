<!doctype html>
<html>
    <head>
       <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>AccessiWheels - Covid-19 Initiative</title>
        <meta property=”og:title” content=”AccessiWheels - Driving people towards equal opportunities” />
        <meta property="og:description" content="AccessiWheels is a social enterprise which enables people with mobility problems to be where they need to be. We connect customers to trained drivers to ensure that they travel safely and conveniently.">
        <meta property=”og:image content=”http://lilypaddigital.co/mediabox/images/UI/accessiwheels-bookingplatform.jpg” />
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

    <?php include "includes/nav_user.php" ?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small uk-container-padded">
            
            <div class="uk-article ">
                <h1 class=" uk-article-title uk-text-bold uk-text-purple uk-margin">AccessiWheels' Covid-19 Initiative</h1>
                <P class="uk-text-lead uk-container-padded">The Vision of AccessiWheels is to drive people towards equal opportunities. <br>As a start up of a persons with disabilities and advocates, we do our best to make the travel essential to bring color to life available. Beyond just the need to go to school and work, to travel for leisure and to where one wants should be accessible. This is what makes us inclusive.</P>

                <p>Sharing our 1st accessible and furthest destination yet which is Tagaytay City on March 14, 2020 (just before Enhanced Community Quarantine or ECQ).</p>

                <div class="uk-grid-small uk-child-width-1-3@m" uk-grid="masonry: true" uk-lightbox="animation: slide" >
                    <div>
                        <a class="uk-inline" href="images/covid/aw_standing-wc.jpg" data-caption="Standing Wheelchair">
                        <img src="images/covid/aw_standing-wc.jpg" /></a>
                    </div>
                    <div>
                        <a class="uk-inline" href="images/covid/aw_tagaytay-ramp.jpg" data-caption="Van ramp for accessibility">
                        <img src="images/covid/aw_tagaytay-ramp.jpg" /></a>
                    </div>
                    <div>
                        <a class="uk-inline" href="images/covid/aw-tagaytay-group.jpg" data-caption="Group photo at Tagaytay">
                        <img src="images/covid/aw-tagaytay-group.jpg" /></a>
                    </div>
                    <div>
                        <a class="uk-inline" href="images/covid/aw-tagaytay-hotel.jpg" data-caption="">
                        <img src="images/covid/aw-tagaytay-hotel.jpg" /></a>
                    </div>
                    <div>
                        <a class="uk-inline" href="images/covid/collage-tagaytay-trip.png" data-caption="">
                        <img src="images/covid/collage-tagaytay-trip.png" /></a>
                    </div>
                    <div>
                        <a class="uk-inline" href="images/covid/aw-tagaytay-testimonial.jpg" data-caption="Testimonial">
                        <img src="images/covid/aw-tagaytay-testimonial.jpg" /></a>
                    </div>
                </div>

                <p class="uk-text-large">“Thank you so much for making the trip possible, it was the highlight of my stay!” -  Mr. Shane Michael Taylor</p>

                <p>When we asked the American music artist/songwriter who needed to return back to the US before quarantine lockdown about one of his songs to dedicate about rising up this COVID-19, it is WARRIOR COWBOY. As the lyrics go, "Spirit cannot be destroyed in a WARRIOR COWBOY" which is what we all need right now. You can hear the full song on YouTube:  </P>
                <div uk-video uk-animation-fade class="uk-text-center" > 
                    <iframe src="https://www.youtube.com/embed/ojRK2LVYZu8" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" width="1920" height="1080" frameborder="0" allowfullscreen uk-responsive uk-video=""></iframe>
                </div>

                <h2 class=" uk-h2 uk-text-bold uk-margin-large-top uk-margin-large-bottom">Our COVID-19 Response</h2>
                <div class="uk-grid">
                    <div class="uk-width-1-3@m" uk-animation-fade>
                        <img src="images/covid/awcovid_dialysis.png" />
                        <p class="uk-text-small uk-margin-remove-top"><a href="https://www.gmanetwork.com/news/news/metro/730006/dialysis-patients-walk-to-hospitals-amid-enhanced-community-quarantine/story/">Source: GMA News Online</a></p>
                    </div>
                    <div class="uk-width-2-3@m">
                        <p class="uk-text-lead">Seeing how the implementation of ECQ which banned all public and commercially available transportation alienated the welfare of vulnerable sector especially those who need to undergo their regular medical procedures who do not have the privilege of owning cars, there is only one thing to do. It is to resort in the bayanihan spirit we have in us. </p>
                    </div>
                </div>
                <h4>The initiative started with call to find brave and kind-hearted volunteer driver with cars who like to help our kababayan with us. We give FREE transportation service thanks to the support of our drivers and donors. </h4>

                <div class="uk-grid">
                    <div class="uk-width-1-3@m" uk-animation-fade>
                        <img src="images/covid/aw-call-for-volunteers.png" />
                    </div>
                    <div class="uk-width-2-3@m">
                        <img src="images/covid/aw-free-pickupdropoff.png" />
                    </div>
                </div>

                <h4 class="uk-text-bold">From March 20 to April 18, we were able to do the following:</h4>

                <p>
                    <ul class="uk-list uk-list-striped">
                        <li><span class="uk-margin-small-right" uk-icon="check"></span> <strong>12 VOLUNTEER DRIVERS</strong> with trips covering 13 Cities/Towns 
                            <div class="uk-grid uk-padding">
                                <div class="uk-width-1-3@m">
                                    <ul class="uk-list">
                                        <li>Quezon City</li>
                                        <li>Caloocan City</li>
                                        <li>Makati City</li>
                                        <li>Marikina City</li>
                                        <li>Manila City</li>
                                        <li>Las Piñas City</li>
                                    </ul>
                                </div>
                                <div class="uk-width-1-3@m">
                                    <ul class="uk-list">
                                        <li>Mandaluyong City</li>
                                        <li>Pasay City</li>
                                        <li>Tanauan & San Jose, Batangas</li>
                                        <li>San Pablo, Laguna</li>
                                        <li>Antipolo,Rizal</li>
                                    </ul>
                                </div>
                            </div>
                            
                        <li><span class="uk-margin-small-right" uk-icon="check"></span> <strong>237 TRIPS</strong> Made (>2,311 km of travel) serving <strong>45 Patients & Health Workers </strong></li>
                        <li><span class="uk-margin-small-right" uk-icon="check"></span> <strong>Php37,147 Raised</strong> where Php15,450 were already provided as allowance to volunteers</li>
                    </ul>
                </p>
                <p><img src="images/covid/aw-brave-volunteers.jpg" /></p>

                <hr>

                <h3 class="uk-text-bold"> Our Partnerships</h3>
                
                <p>The Pasig City government partnered with AccessiWheels to develop the system helping their residents needing medical attention.</p>

                <p>
                    <img src="https://scontent.fcrk2-1.fna.fbcdn.net/v/t1.0-9/91474516_921303921635634_1502290890540974080_n.jpg?_nc_cat=109&_nc_sid=110474&_nc_ohc=8sytP4vQN_UAX-WM4ZP&_nc_ht=scontent.fcrk2-1.fna&oh=96ff82e3e52c226a9fb40457ed9acbfc&oe=5EC0EE26" />
                </p>
                <p><a href="https://www.facebook.com/PasigTransport/photos/a.572231723209524/921303918302301/?type=3&theater">View source</a>
                </p>

                <hr>

                <h3 class="uk-text-bold"> Reviews and Testimonials</h3>

                <div class="uk-grid-small uk-child-width-1-2@s uk-child-width-1-3@m uk-padding" uk-grid="masonry: true" uk-lightbox="animation: slide">
                    <div>
                        <a class="uk-container uk-padding" href="#modal-isip" uk-toggle>
                            <img src="images/covid/aw-isip-review.png" />
                            <p class="uk-text-bold uk-text-purple">Innovation for Social Impact Partnership (ISIP)</p>
                        </a>

                        <div id="modal-isip" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">Innovation for Social Impact Partnership (ISIP)</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <img src="images/covid/aw-isip-review.png" />
                                    <img src="images/covid/aw-isip-review-article.png" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a class="uk-container uk-padding" href="#modal-us" uk-toggle>
                            <p><img src="images/covid/review-us-embassy.png" /></p>
                            <p class="uk-text-bold uk-text-purple">U.S. Embassy</p>
                        </a>

                        <div id="modal-us" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">U.S. Embassy in the Philippines</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FUSEmbassyPH%2Fposts%2F10158370941674623&width=500" width="500" height="654" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a class="uk-container uk-padding" href="#modal-makesense" uk-toggle>
                            <p><img src="images/covid/review-makesense.png" /></p>
                            <p class="uk-text-bold uk-text-purple">Make Sense</p>
                        </a>

                        <div id="modal-makesense" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">Make Sense</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <p><img src="images/covid/review-makesense.png" /></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a class="uk-container uk-padding" href="#modal-levi" uk-toggle>
                            <p><img src="images/covid/aw-testimonial02.png" /></p>
                            <p class="uk-text-bold uk-text-purple">Testimonial from Levi Icawalo</p>
                        </a>

                        <div id="modal-levi" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">From Levi Icawalo</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fnomad080819%2Fposts%2F3321782534502106&width=500" width="500" height="540" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a class="uk-container uk-padding" href="#modal-lorie" uk-toggle>
                            <p><img src="images/covid/aw-testimonial01.png" /></p>
                            <p class="uk-text-bold uk-text-purple">Testimonial from Lorie Dalumpines</p>
                        </a>

                        <div id="modal-lorie" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">From Lorie Dalumpines</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Florelyn.resma.7%2Fposts%2F928922274220697&width=500" width="500" height="802" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a class="uk-container uk-padding" href="#modal-flipscience" uk-toggle>
                            <p><img src="images/covid/review-flipscience.png" /></p>
                            <p class="uk-text-bold uk-text-purple">Flip Science</p>
                        </a>

                        <div id="modal-flipscience" uk-modal>
                            <div class="uk-modal-dialog">

                                <button class="uk-modal-close-default" type="button" uk-close></button>

                                <div class="uk-modal-header">
                                    <h2 class="uk-modal-title">Flip Science</h2>
                                </div>

                                <div class="uk-modal-body" uk-overflow-auto>
                                    <p><img src="images/covid/review-flipscience.png" /></p>
                                    <p><a href="https://www.flipscience.ph/news/philippine-startups-covid-19/?fbclid=IwAR2bGIVVlPCDDY8dh-avUyXJq7I3YvURiVlzNpl0Dd0mrlAGbrCl9zdTAm0">View source</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end of reviews and testimonials section -->

                <h3 class="uk-text-bold">AccessiWheels Booking Web App</h3>
                <p>In partnership with <a href="http://lilypaddigital.co" class="uk-text-bold">Lily Pad Digital Solutions, Inc.</a>, we're also building our booking web application.</p>
                <p><img src="images/covid/booking-app.png" /></p>
                <a href="https://book.accessiwheels.com/" class="uk-button uk-button-primary uk-align-center">AccessiWheels Booking Web App</a>


            </div> <!-- end -->


        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>