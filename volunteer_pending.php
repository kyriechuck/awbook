<?php
    session_start(); //session start

    // Include config file
    include "config.php";
    
    // Attempt select query execution
    $sql = "SELECT * FROM bookings WHERE status='pending' or status='received' ORDER BY created_at DESC";
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-volunteers.php");
        exit;
    }

    include "includes/nav_volunteer.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
             
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">Pending Ride Requests</h1>
                </div>
                <div class="uk-width-1-2@m ">
                    <a href="volunteer_addride.php" class="uk-button uk-button-primary uk-align-right">Add New Ride</a>
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-3-5@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li class="uk-active"><a href="volunteer_pending.php">Pending Rides</a></li>
                        <li ><a href="volunteer_scheduled.php">Scheduled Rides</a></li>
                        <li><a href="volunteer_completed.php">Completed Rides</a></li>
                        <li hidden><a href="admin-rides_cancelled.php">Cancelled Rides</a></li>
                    </ul>
                </div>
                <div class="uk-width-2-5@m ">
                    <div class="uk-grid">
                        <div class="uk-width-2-3@m">
                            <form action="volunteer_search.php" class="uk-search uk-search-default uk-align-left uk-flex-inline" method="POST">
                                <div class="">
                                    <span uk-search-icon></span>
                                    <input name="searchrides" class="uk-search-input uk-width-auto uk-text-small " type="search" placeholder="Search Rides">
                                </div>
                                <input class="uk-button uk-button-primary" type="submit" value="Go">
                            </form>
                        </div>
                        <div class="uk-width-1-3@m">
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <p>These are rides with pending and received status. </p>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                <div class="uk-overflow-auto">
                    <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                        <thead>
                                <tr>
                                <th class="uk-width-small" >Passenger</th>
                                <th class="uk-width-small">Pick-up Point</th>
                                <th class="uk-width-small">Drop-off Point</th>
                                <th class="uk-width-small">PU Date and Time</th>
                                <th class="uk-width-small">Date Added</th>
                                <th class="uk-width-small">Message</th>
                                <th class="uk-table-shrink">Status</th>
                                <th class="uk-table-shrink">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($result = mysqli_query($link, $sql)){
                                    if(mysqli_num_rows($result) > 0){

                                        while($row = mysqli_fetch_array($result)){
                                            $id = $row["id"];
                                            $passcode = $row["passcode"];
                                            $category = $row["category"];
                                            $name = $row["firstname"] ." ". $row['lastname'];
                                            $dropoff = $row["dropoff"];
                                            $pickup = $row["pickup"];
                                            $date_old = $row["date"];
                                            $date = date("M, d, Y", strtotime($date_old));
                                            $time = $row["time"];
                                            $status = $row["status"];
                                            $message = $row["message"];
                                            $budget = $row["budget"];
                                            $date_added_old = $row["created_at"];
                                            $date_added = date("M, d, Y", strtotime($date_added_old));


                                            echo '<tr> 
                                                    <td class="uk-text-wrap">'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='volunteer_updateride.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $category.'</td>
                                                    <td class="uk-text-wrap">'.$pickup.'</td>  
                                                    <td class="uk-text-wrap">'.$dropoff.'</td>
                                                    <td class="uk-text-wrap">'.$date. '<br>' . $time .'</td>
                                                    <td class="uk-text-wrap">'.$date_added.'</td>  
                                                    <td class="uk-text-wrap">'.$message.'</td>  
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$status . '</td> 
                                                    <td class="uk-text-wrap">' . "<a href='volunteer_viewride.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'><span class='uk-icon' uk-icon='push'></span></a>" . "<a href='volunteer_updateride.php?id=". $row['id'] ."' title='Update' data-toggle='tooltip'><span class='uk-icon' uk-icon='file-edit'></span></a>". '</td>
                                                </tr>';
                                        }
                                        // Free result set
                                        mysqli_free_result($result);
                                    } else{
                                        echo "No records matching your query were found.";
                                    }
                                } else{
                                    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                }
                            
                            // Close connection
                            mysqli_close($link);
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>