<?php

session_start(); //session start

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
// Processing form data when form is submitted
// Check input errors before inserting in database
// Prepare an update statement
// Bind variables to the prepared statement as parameters
// Set parameters
// Attempt to execute the prepared statement
// Close statement
// Close connection

// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];

    $category = trim($_POST["category"]);
    $passcode = trim($_POST["passcode"]);
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST['lastname']);
    $phone = trim($_POST["phone"]);
    $pickup = trim($_POST["pickup"]);
    $dropoff = trim($_POST["dropoff"]);
    $date = trim($_POST["date"]);
    $time = trim($_POST["time"]);
    $message = trim($_POST["message"]);
    $budget = trim($_POST["budget"]);
    $driver_id = trim($_POST["driver_id"]);
    $distance = trim($_POST["distance"]);
    $status = trim($_POST["status"]);


    // Prepare an update statement
    $sql = "UPDATE bookings SET category='$category', passcode='$passcode', firstname='$firstname', lastname='$lastname', phone='$phone', pickup='$pickup', dropoff='$dropoff', date='$date', time='$time', message='$message', budget='$budget', driver_id='$driver_id', distance='$distance', status='$status' WHERE id=$id ";
        
    if($stmt = mysqli_prepare($link, $sql)){

        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "sssi", $param_category, $param_passcode, $param_firstname, $param_lastname,  $param_phone, $param_pickup, $param_dropoff, $param_date, $param_time, $param_message, $param_budget, $param_driver_id, $param_distance, $param_status, $param_id);
        
        // Set parameters
        $param_category = $category;
        $param_passcode = $passcode;
        $param_firstname = $firstname;
        $param_lastname = $lastname;
        $param_phone = $phone;
        $param_pickup = $pickup;
        $param_dropoff = $dropoff;
        $param_date = $date;
        $param_time = $time;
        $param_message = $message;
        $param_budget = $budget;
        $param_driver_id = $driver_id
        $param_distance = $distance;
        $param_status = $status;
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records updated successfully. Redirect to landing page
            header("location: successful.php");
            exit();
        } else{
            echo "Something went wrong. Please try again later.";
        }
    }
        
    // Close statement
    mysqli_stmt_close($stmt);

    // Close connection
    mysqli_close($link);
    

} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM bookings WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $id = $row["id"];
                    $category = $row["category"];
                    $passcode = $row["passcode"];
                    $firstname = $row["firstname"];
                    $lastname = $row['lastname'];
                    $phone = $row["phone"];
                    $pickup = $row["pickup"];
                    $dropoff = $row["dropoff"];
                    $date_old = $row["date"];
                    $date = date("M, d, Y", strtotime($date_old));
                    $time = $row["time"];
                    $message = $row["message"];
                    $budget = $row["budget"];
                    $driver_id = $row["driver_id"];
                    $distance = $row["distance"];
                    $status = $row["status"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
// end update ride


?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>
                <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom uk-margin-remove-top">Update Ride Details</h1>
                <P class="uk-text uk-text uk-margin-remove-top">Edit values then submit to update this booking request.</P>
                
                <form class="uk-form uk-container-padded"  action="admin-ride-update.php" method="POST">
                     
                    <fieldset class="uk-fieldset">
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" value="<?php echo $firstname; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">First Name</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $lastname; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Last Name</p>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="passcode" class="uk-input uk-text-bold" type="text" placeholder="Passenger Code" value="<?php echo $passcode; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Passenger Code</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="category" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $category; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Category: Frontliner / Patient</p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" value="<?php echo $phone; ?>"  >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Contact Number</p>
                            </div>
                        </div>
                        <hr>
                        <div class="uk-margin uk-width-auto">
                            <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="Pick-up Point" value="<?php echo $pickup; ?>" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Pick-up Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="Drop-off Point" value="<?php echo $dropoff; ?>" >
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Drop-off Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                    
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="date" class="uk-input" type="date" placeholder="" value="<?php echo $date_old; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <select name="time" class="uk-select" value="<?php echo $time; ?>" >
                                    <option>06:00 AM</option>
                                    <option>06:30 AM</option>
                                    <option>07:00 AM</option>
                                    <option>07:30 AM</option>
                                    <option>08:00 AM</option>
                                    <option>08:30 AM</option>
                                    <option>09:00 AM</option>
                                    <option>09:30 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:30 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:30 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:30 PM</option>
                                    <option>01:00 PM</option>
                                    <option>01:30 PM</option>
                                    <option>02:00 PM</option>
                                    <option>02:30 PM</option>
                                    <option>03:00 PM</option>
                                    <option>03:30 PM</option>
                                    <option>04:00 PM</option>
                                    <option>04:30 PM</option>
                                    <option>05:00 PM</option>
                                    <option>05:30 PM</option>
                                    <option>06:00 PM</option>
                                    <option>06:30 PM</option>
                                    <option>07:00 PM</option>
                                    <option>07:30 PM</option>
                                    <option>08:00 PM</option>
                                    <option>08:30 PM</option>
                                    <option>09:00 PM</option>
                                    <option>09:30 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:30 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:30 PM</option>
                                    <option>12:00 AM</option>
                                </select>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Time</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="budget" class="uk-input uk-text-bold" type="number" value="<?php echo $budget; ?>">
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Budget</p>
                            </div>
                        </div> 
                        <div class="uk-grid uk-margin">
                            
                            <div class="uk-width-1-3@m" >
                                <select name="status" class="uk-select uk-alert-warning" value="<?php echo $status; ?>">
                                    <option value="pending" <?=($status == "pending" ? "selected": "") ?> >Pending</option>
                                    <option value="received" <?=($status == "received" ? "selected": "") ?> >Received</option>
                                    <option value="scheduled" <?=($status == "scheduled" ? "selected": "") ?> >Scheduled</option>
                                    <option value="completed" <?=($status == "completed" ? "selected": "") ?> >Completed</option>
                                    <option value="bikerequest" <?=($status == "bikerequest" ? "selected": "") ?> hidden> Bike Request</option>
                                    <option value="bikelent" <?=($status == "bikelent" ? "selected": "") ?> hidden> Bike Lent</option>
                                    <option value="bikereturned" <?=($status == "bikereturned" ? "selected": "") ?> hidden> Bike Returned</option>
                                    <option value="Cancelled by Driver" <?=($status == "cancelleddriver" ? "selected": "") ?> >Cancelled by Driver</option>
                                    <option value="Cancelled by Rider" <?=($status == "cancelledrider" ? "selected": "") ?> >Cancelled by Rider</option>
                                </select>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Status</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <select class="uk-select" name="driver_id" value="<?php echo $driver_id; ?>">
                                    
                                    <?=
                                        // Attempt select query execution
                                        $sql = "SELECT id, firstname, lastname from drivers";

                                        if($result = mysqli_query($link, $sql)){
                                            if(mysqli_num_rows($result) > 0){

                                                while($row = mysqli_fetch_assoc($result)){

                                                    echo '<option value="'.$row["id"].'" >'.$row["firstname"].' '.$row["lastname"].'</option>';
                                                }
                                                // Free result set
                                                mysqli_free_result($result);
                                            }
                                        } 
                                    
                                    // Close connection
                                    mysqli_close($link);
                                    ?>
                                    
                                </select>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Assigned Driver</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="distance" class="uk-input uk-text-bold uk-alert-success" type="number" step="0.1" value="<?php echo $distance; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Trip Distance</p>
                            </div>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <textarea name="message" class="uk-textarea" name="message" rows="10" value="" placeholder="Enter your message"><?php echo $message; ?></textarea>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Please enter any additional instructions or special request in the box above. E.g. Return Time, Need of vehicle going back to the hospital, etc.</p>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                        <input class="uk-button uk-button-primary uk-margin-top uk-width-1-1" type="submit" value="Update" name="submit">
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Rides</a></li>
                <li><a href="#">Update Ride</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>
