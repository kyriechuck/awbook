<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['Username']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">My Rides</h1>
                    <p>Browse all your ride activity and ride details here.</p>
                </div>
                <div class="uk-width-1-2@m">
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-2-3@m">
                    <ul class="uk-horizontal-menu uk-nav uk-text-bold">
                        <li class="uk-active"><a href="my-rides-pending.php">Pending Rides</a></li>
                        <li ><a href="my-rides-scheduled.php">Scheduled Rides</a></li>
                        <li><a href="my-rides-finished.php">Finished Rides</a></li>
                    </ul>
                </div>
                <div class="uk-width-1-3@m ">
                    <div class="uk-grid">
                        <div class="uk-width-1-2@m">
                            <form class="uk-search uk-search-default uk-align-right">
                                <span uk-search-icon></span>
                                <input class="uk-search-input uk-text-small " type="search" placeholder="Search Rides">
                            </form>
                        </div>
                        <div class="uk-width-1-2@m">
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <!-- main content -->
            <div class="uk-container-small uk-container-padded">
                <p class="uk-text-small uk-margin-remove">Go Back</p>
                <h2 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Ride Details</h2>
                <p class="uk-margin-remove-top">Booking ID: AHGDB881237</p>
                <hr>
                <div class="uk-grid ">
                    <!-- left column -->
                    <div class="uk-width-2-3@m">
                        <div class="uk-grid">
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">TO</p>
                                <h3 class="uk-margin-small uk-text-bold uk-text-purple">East Avenue Medical Center, Quezon City</h3>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">FROM</p>
                                <h3 class="uk-margin-small uk-text-bold uk-text-purple">Maalalahanin, Teachers Village, Quezon City</h3>
                            </div>
                        </div>

                        <div class="uk-grid">
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">ASSIGNED DRIVER</p>
                                <h4 class="uk-margin-small">Juan Dela Cruz</h4>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP TIME</p>
                                <h4 class="uk-margin-small ">10:30 AM</h4>
                            </div>
                        </div>
                        
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">MESSAGE TO DRIVER</p>
                                <p class="uk-margin-small">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                    </div>

                    <!-- right column -->
                    <div class="uk-width-1-3@m">
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">CAR TYPE</p>
                                <h4 class="uk-margin-small ">Sedan</h4>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">DATE</p>
                                <h4 class="uk-margin-small">Wednesday, April 10, 2020</h4>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">TOTAL PRICE</p>
                                <h2 class="uk-margin-small uk-text-bold uk-text-green">Php 360</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>