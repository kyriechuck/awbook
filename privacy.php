<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

    <?php include "includes/nav_user.php" ?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container uk-container-small">
            <div class="uk-article">
                <h1 class=" uk-article-title uk-text-bold uk-text-purple uk-margin-remove-bottom">Privacy Policy</h1>
                
                <P class="uk-text uk-text-lead">AccessiWheels highly values your information and makes sure your data is secured and protected. This privacy policy explains how we collect, use, and secure your personal identifiable information. This policy covers the use of AccessiWheels platform for booking which may include facebook, website, app, and email and transfer of needed data to our trainied partners in order to provide rides to customers.</P>

                <h3 class="uk-text-bold">What We Collect</h3>
                <p class="uk-text">Upon communication with us which can include email, Facebook Messenger, printed and online transportation form or website or enrollment of service through your doctor, we collect your name, age, origin address, destination address, schedule for pick up, phone number, email address (if you sent an email), facebook name (if you messaged our page/profile), preferred vehicle choices, and specific assistance needed/request.</p>

                <h3 class="uk-text-bold">Why We Collect</h3>
                <p>We collect information to facilitate your booking, improve our services and ensure safe and best customer experience. We need the information to provide customer support, send transaction details, send notifications, invite others to use our service, and track our performance.</p>
                
                <h3 class="uk-text-bold">Disclosure</h3>
                <p>Your profile includes basic information about you including your anme, address, contact number, and email address which we share to the other party to facilitate the transaction. We may also share the information to vendors and service providers who are working with us to improve service to people with mobility problems.</p>
                    
            </div>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>