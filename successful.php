<?php
    session_start(); //session start
    
    // Include config file
    include "config.php";
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small uk-text-center ">
            <div class="uk-margin uk-container-padded">
                <h1 class="uk-text-bold uk-text-green">Successful!</h1>
                <h3 class="uk-text-bold uk-text-purple uk-margin-remove-top">Records have been updated. </h3>
            </div>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>