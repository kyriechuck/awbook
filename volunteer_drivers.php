<?php
    session_start(); //session start
   
    // Include config file
    require_once "config.php";
    
    // Attempt select query execution
    $sql = "SELECT * FROM drivers WHERE id <> 0 ORDER BY firstname ASC";
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-volunteer.php");
        exit;
    }

    include "includes/nav_volunteer.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
            
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">Drivers</h1>
                    <p>Below are all the listed partner drivers of AccessiWheels</p>
                </div>
                <div class="uk-width-1-2@m " hidden>
                    <a href="admin-add-driver.php" class="uk-button uk-button-primary uk-align-right">Add New Driver</a>
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-3-5@m">
                    <form action="volunteer_searchdrivers.php" class="uk-search uk-search-default uk-align-left uk-flex-inline" method="POST">
                        <div class="">
                            <span uk-search-icon></span>
                            <input name="searchdrivers" class="uk-search-input uk-width-large uk-text-small " type="search" placeholder="Search Rides">
                        </div>
                        <input class="uk-button uk-button-primary" type="submit" value="Go">
                    </form>
                </div>
                <div class="uk-width-2-5@m">
                    <div class="uk-margin uk-align-right">
                        <button class="uk-button uk-button-default uk-text-right" type="button">Display: 5 <span uk-icon="triangle-down"></span></button>
                        <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="#">5</a></li>
                                <li><a href="#">10</a></li>
                                <li><a href="#">15</a></li>
                                <li><a href="#">25</a></li>
                                <li><a href="#">50</a></li>
                                <li><a href="#">100</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                
                <div class="uk-overflow-auto">
                    <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                        <thead>
                                <tr>
                                <th class="uk-width-small" >Driver</th>
                                <th class="uk-width-small">Area of Coverage</th>
                                <th class="uk-width-small">Contact Details</th>
                                <th class="uk-width-small" >Availability</th>
                                <th class="uk-width-small">Car Type</th>
                                <th class="uk-table-shrink">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($result = mysqli_query($link, $sql)){
                                    if(mysqli_num_rows($result) > 0){
                                        
                                        while($row = mysqli_fetch_array($result)){
                                            $id = $row['id'];
                                            $name = $row["firstname"] ." ". $row['lastname'];
                                            $address = $row["address"];
                                            $phone = $row["phone"];
                                            $email = $row["email"];
                                            $cartype = $row["cartype"];
                                            $carmodel = $row["carmodel"];
                                            $date_added_old = $row["created_at"];
                                            $date_added = date("M, d, Y", strtotime($date_added_old));
                                            $status = $row["status"];
                                            $facebook = $row["facebook"];
                                            $availability = $row["availability"];

                                            echo '<tr> 
                                                    <td class="uk-table-link uk-text-purple uk-text-bold">'. $name .'</td> 
                                                    <td class="uk-text-wrap">'.$address.'</td> 
                                                    <td class="uk-text-wrap">'.$phone. '<br>' . $facebook .'</td>
                                                    <td class="uk-text-wrap">'.$availability.'</td> 
                                                    <td class="uk-text-wrap">'. '<span class=" uk-text-uppercase">'.$cartype.'</span>'. '<br>'. $carmodel. '</td> 
                                                    <td class="uk-text-wrap uk-text-capitalize">' .$status . '</td> 
                                                </tr>';
                                        }
                                        echo "</table>";
                                        // Free result set
                                        mysqli_free_result($result);
                                    } else{
                                        echo "No records matching your query were found.";
                                    }
                                } else{
                                    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                }
                            
                            // Close connection
                            mysqli_close($link);
                            ?>
                        </tbody>
                </div>
            </div>
            <!-- end: switcher content -->
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">My Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>