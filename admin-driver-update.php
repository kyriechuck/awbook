<?php

session_start(); //session start

// Include config file
require_once "config.php";

// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    $firstname = trim($_POST["firstname"]);
    $lastname = trim($_POST["lastname"]);
    $phone = trim($_POST["phone"]);
    $drivercode = trim($_POST["drivercode"]);
    $address = trim($_POST["address"]);
    $cartype = trim($_POST["cartype"]);
    $carmodel = trim($_POST["carmodel"]);
    $status = trim($_POST["status"]);
    $facebook = trim($_POST["facebook"]);
    $availability = trim($_POST["availability"]);


    // Prepare an update statement
    $sql = "UPDATE drivers SET firstname='$firstname', lastname='$lastname', phone='$phone', drivercode='$drivercode', address='$address', facebook='$facebook', availability='$availability', status='$status', cartype='$cartype', carmodel='$carmodel' WHERE id=$id ";
        
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "sssi", $param_firstname, $param_lastname,  $param_phone, $param_drivercode, $param_address, $param_facebook, $param_availability,  $param_cartype, $param_carmodel, $param_status, $param_id);
        
        // Set parameters
        $param_firstname = $firstname;
        $param_lastname = $lastname;
        $param_phone = $phone;
        $param_drivercode = $drivercode;
        $param_address = $addres;
        $param_cartype = $cartype;
        $param_carmodel = $carmodel;
        $param_status = $status;
        $param_facebook = $facebook;
        $param_availability = $availability;
        $param_id = $id;
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records updated successfully. Redirect to landing page
            $_SESSION['success'] = " <div class='uk-padding uk-alert-success uk-text-large uk-text-center' uk-alert>
                                <a class='uk-alert-close' uk-close></a>
                                <p>You have successfully updated the details of driver <span class='uk-text-bold'>$firstname $lastname</span> !</p>
                            </div> ";
        } else{
            echo "Something went wrong. Please try again later.";
        }
    }
        
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM drivers WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $id = $row["id"];
                    $firstname = $row["firstname"];
                    $lastname = $row['lastname'];
                    $phone = $row["phone"];
                    $address = $row["address"];
                    $facebook = $row["facebook"];
                    $availability = $row["availability"];
                    $status = $row["status"];
                    $car_model = $row["carmodel"];
                    $car_type = $row["cartype"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>

<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                <?php if (isset($_POST['submit'])) : ?>
                    <div class="error success" >
                        <h3>
                            <?php  echo $_SESSION['success']; ?>
                        </h3>
                    </div>
                <?php endif ?>
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>
                <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Update Driver Details</h1>
                <P class="uk-text uk-text uk-margin-remove-top">Edit values then submit to update driver details.</P>
                
                <form class="uk-form uk-container-padded" name="book-pudo" action="admin-driver-update.php" method="POST">

                    <fieldset class="uk-fieldset">
                        <h3 class="uk-text-bold">Personal Details</h3>
                        <p>Please fill out all fields.</p>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="firstname" class="uk-input uk-text-bold" type="text" value="<?php echo $firstname; ?>" placeholder="First Name" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter first name</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="lastname" class="uk-input uk-text-bold" type="text" value="<?php echo $lastname; ?>" placeholder="Last Name" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter last name</p>
                            </div>
                        </div>

                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                       
                                <input name="address" class="uk-input uk-text-bold" type="text" value="<?php echo $address; ?>" placeholder="Coverage" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Area of coverage.</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="drivercode" class="uk-input uk-text-bold" type="text" value="<?php echo $drivercode; ?>" placeholder="" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">6-digit Driver Code</p>
                            </div>

                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="facebook" class="uk-input uk-text-bold" type="text" value="<?php echo $facebook; ?>" placeholder="Facebook URL" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter facebook profile url</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input name="availability" class="uk-input uk-text-bold" type="text" value="<?php echo $availability; ?>" placeholder="Driver availability" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter availability</p>
                            </div>
                        </div>
                    
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="phone" class="uk-input uk-text-bold" type="text" value="<?php echo $phone; ?>" placeholder="Contact Number" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter contact number</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <select name="cartype" class="uk-select" value="<?php echo $car_type; ?>">
                                    <option value="sedan" <?=($cartype == "sedan" ? "selected": "") ?> >Sedan</option>
                                    <option value="van" <?=($cartype == "van" ? "selected": "") ?> >Van</option>
                                    <option value="auv" <?=($cartype == "auv" ? "selected": "") ?> >AUV</option>
                                    <option value="ambulance" <?=($cartype == "ambulance" ? "selected": "") ?> >Ambulance</option>
                                    <option value="uv express" <?=($cartype == "uv express" ? "selected": "") ?> >UV Express</option>
                                </select>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Car Type</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="carmodel" class="uk-input uk-text-bold" type="text" placeholder="Vehicle Model" value="<?php echo $car_model; ?>" >
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Enter vehicle model</p>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-width-1-3@m" >
                                <select name="status" class="uk-select uk-alert-warning" value="<?php echo $status; ?>">
                                    <option value="active" <?=($status == "active" ? "selected": "") ?> >Active</option>
                                    <option value="inactive" <?=($status == "inactive" ? "selected": "") ?> >Inactive</option>
                                    <option value="warning" <?=($status == "warning" ? "selected": "") ?> >Warning</option>
                                </select>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Driver Status</p>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input class="uk-button uk-button-primary uk-width-1-1 uk-margin-top" type="submit" value="Submit" name="submit">
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>
