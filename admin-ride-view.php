<?php
    session_start(); //session start
    
    // Include config file
    require_once "config.php";
    
    // Attempt select query execution
    $sql = "SELECT 
        bookings.*, drivers.firstname as driverFirstname, drivers.lastname as driverLastname
        FROM drivers
        JOIN bookings on drivers.id = bookings.driver_id
        WHERE bookings.id = ? ";
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php 
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login-admin.php");
        exit;
    }

    include "includes/nav_admin.php" 
?>

<?php
// Check existence of id parameter before processing further
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = trim($_GET["id"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
    
            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $id = $row["id"];
                $category = $row["category"];
                $passcode = $row["passcode"];
                $name = $row["firstname"] . " " . $row["lastname"];
                $firstname = $row["firstname"];
                $lastname = $row["lastname"];
                $phone = $row["phone"];
                $email = $row["email"];
                $pickup = $row["pickup"];
                $dropoff = $row["dropoff"];
                $date = $row["date"];
                $time = $row["time"];
                $message = $row["message"];
                $budget = $row["budget"];
                $driver_id = $row["driver_id"];
                $driverFirstname = $row['driverFirstname'];
                $driverLastname = $row['driverLastname'];
                $status = $row["status"];
                $distance = $row["distance"];
                $date_addedold = $row["created_at"];
                $date_added = date("M, d, Y", strtotime($date_addedold));

                $driverFirstname = $row['driverFirstname'];
                $driverLastname = $row['driverLastname'];
            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">

            <!-- main content -->
            <div class="uk-container uk-container-small">
                <a href="admin-rides_pending.php"><p class="uk-text-small">Go Back</p></a>
                <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom"><?php echo $name; ?></h1>
                <P class="uk-text uk-text uk-margin-remove-top">Booking request summary view</P>
                <p class="uk-text uk-text-bold uk-margin-remove-top">Date Added: <?php echo $date_added; ?></p>
                
                <form class="uk-form uk-container-padded"  action="admin-ride-update.php" method="POST">
                    
                    <fieldset class="uk-fieldset">
                        
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-2-5@m" >
                                <label>First Name</label>
                                <input name="firstname" class="uk-input uk-text-bold" type="text" placeholder="First Name" value="<?php echo $firstname; ?>" disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">First Name</p>
                            </div>
                            <div class="uk-width-2-5@m" >
                                <label>Last Name</label>
                                <input name="lastname" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $lastname; ?>" disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Last Name</p>
                            </div>
                            <div class="uk-width-1-5@m" >
                                <label>Passenger Code</label>
                                <input name="passcode" class="uk-input uk-text-bold" type="text" placeholder="Passenger Code" value="<?php echo $passcode; ?>" disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Passenger Code</p>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <label>Category: Frontliner / Patient</label>
                                <input name="category" class="uk-input uk-text-bold" type="text" placeholder="Last Name" value="<?php echo $category; ?>" disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Category: Frontliner / Patient</p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <label>Contact Number</label>
                                <input name="phone" class="uk-input uk-text-bold" type="text" placeholder="Contact Number" value="<?php echo $phone; ?>"  disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Contact Number</p>
                            </div>
                            <div class="uk-width-1-3@m">
                                <label>Email Address</label>
                                <input name="email" class="uk-input uk-text-bold" type="email" placeholder="Email Address" value="<?php echo $email; ?>"  disabled>
                                <p hidden class="uk-text-small uk-margin-remove-top uk-text-muted">Email Address</p>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="uk-margin uk-width-auto">
                            <input name="pickup" class="uk-input uk-text-bold" type="text" placeholder="Pick-up Point" value="<?php echo $pickup; ?>" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Pick-up Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                        <div class="uk-margin uk-width-auto">
                            <input name="dropoff" class="uk-input uk-text-bold" type="text" placeholder="Drop-off Point" value="<?php echo $dropoff; ?>" disabled>
                            <p class="uk-text-small uk-margin-remove-top uk-text-muted">Drop-off Point: Please provide complete street address, hotel / hospital / airport name, and city.</p>
                        </div>
                    
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-2@m" >
                                <input name="date" class="uk-input" type="date" placeholder="" value="<?php echo $date; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Date of Pick-up</p>
                            </div>
                            <div class="uk-width-1-2@m" >
                                <input class="uk-input" type="text" value="<?php echo $time; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Select Time</p>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-1-3@m" >
                                <input name="budget" class="uk-input uk-text-bold" type="number" value="<?php echo $budget; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Budget</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="status" class="uk-input uk-text-bold" type="text" value="<?php echo $status; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Status</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="driver" class="uk-input uk-text-bold" type="text" value="<?php echo $driverFirstname .' '. $driverLastname; ?>" placeholder="Driver Name" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Assigned Driver</p>
                            </div>
                        </div>
                        <div class="uk-grid uk-margin">
                            <div class="uk-width-2-3@m" >
                                <input name="message" class="uk-input uk-text-bold" type="text" value="<?php echo $message; ?>" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Additional Instructions</p>
                            </div>
                            <div class="uk-width-1-3@m" >
                                <input name="distance" class="uk-input uk-text-bold" type="text" value="<?php echo $distance; ?>" placeholder="Distance in KM" disabled>
                                <p class="uk-text-small uk-margin-remove-top uk-text-muted">Trip Distance</p>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>

                        <?php echo "<a class='uk-button uk-button-primary uk-margin-top uk-width-1-1' href='admin-ride-update.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>Edit Details <span class='uk-icon' uk-icon='file-edit'></span></a>"; ?>
                    </fieldset>
                        
                    
                </form>
            </div>
            <!-- main content -->
            
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
                <li><a href="#">Details</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>