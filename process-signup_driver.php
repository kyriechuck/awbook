<?php

session_start(); //session start

// Include config file
require_once "config.php";

 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    $firstname = trim($_SESSION["firstname"]);
    $lastname = trim($_SESSION["lastname"]);
    $phone = trim($_SESSION["phone"]);
    $address = trim($_SESSION["address"]);
    $cartype = trim($_SESSION["cartype"]);
    $carmodel = trim($_SESSION["carmodel"]);
    $status = trim($_SESSION["status"]);
    $facebook = trim($_SESSION["facebook"]);
    $availability = trim($_SESSION["availability"]);
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) ){
        
        // Prepare an insert statement ('$username', '$password', '$firstname', '$lastname', $email, $phone, $address, $carmodel)
        $sql = "INSERT 
        INTO drivers (firstname, lastname, phone, address, cartype, carmodel, status, facebook, availability) 
        VALUES ( '$firstname', '$lastname', '$phone', '$address', '$cartype', '$carmodel', '$status', '$facebook', '$availability')";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password, $param_firstname, $param_lastname, $param_email, $param_phone, $param_address, $param_cartype, $param_carmodel, $param_status, $param_facebook, $param_availability);
            
            // Set parameters
            $param_firstname = $firstname;
            $param_lastname = $lastname;
            $param_phone = $phone;
            $param_address = $addres;
            $param_cartype = $cartype;
            $param_carmodel = $carmodel;
            $param_status = $status;
            $param_facebook = $facebook;
            $param_availability = $availability;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: admin-add-driver_submitted.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    
    // Close connection
    mysqli_close($link);
}
?>
