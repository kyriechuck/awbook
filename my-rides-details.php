<?php
	session_start(); //session start
?>
<!doctype html>
<html>
    <head>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['Username']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
           
            <div class="uk-container-small uk-align-center">
                <h1 class="uk-text-bold uk-text-purple">Ride Details</h1>
                <p>Booking ID: AHGDB881237</p>
                <hr>
                <div class="uk-grid ">
                    <!-- left column -->
                    <div class="uk-width-2-3@m">
                        <div class="uk-grid">
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">TO</p>
                                <h3 class="uk-margin-small uk-text-bold">East Avenue Medical Center, Quezon City</h3>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">FROM</p>
                                <h3 class="uk-margin-small uk-text-bold ">Maalalahanin, Teachers Village, Quezon City</h3>
                            </div>
                        </div>

                        <div class="uk-grid">
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">ASSIGNED DRIVER</p>
                                <h4 class="uk-margin-small">Juan Dela Cruz</h4>
                            </div>
                            <div class="uk-width-1-2@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">PICK-UP TIME</p>
                                <h4 class="uk-margin-small ">10:30 AM</h4>
                            </div>
                        </div>
                        
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">MESSAGE TO DRIVER</p>
                                <h4 class="uk-margin-small">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
                            </div>
                        </div>
                    </div>

                    <!-- right column -->
                    <div class="uk-width-1-3@m">
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">CAR TYPE</p>
                                <h4 class="uk-margin-small ">Sedan</h4>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">DATE</p>
                                <h4 class="uk-margin-small">Wednesday, April 10, 2020</h4>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-1@m">
                                <p class=" uk-margin-remove-bottom uk-text-small uk-text-bold">TOTAL PRICE</p>
                                <h2 class="uk-margin-small uk-text-bold uk-text-green">Php 360</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">My Rides</a></li>
                <li><a href="#">Ride Details</a></li>
            </ul>

            
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>