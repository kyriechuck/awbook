<?php
    session_start(); //session start

    // Include config file
    include "config.php";
    
    // Attempt select query execution
    $sql = "SELECT * FROM bookings WHERE status='pending' ORDER BY created_at ASC";
    $pending = mysqli_num_rows(mysqli_query($link, "SELECT * FROM bookings where status='pending'"));
?>

<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script>
        <title>AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php

if(!isset($_SESSION['loggedin']))//if session not found use default header
{
    require_once("includes/nav_user.php");
}else{  
    require_once("includes/nav_user-loggedin.php");
}
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
		<div class="uk-container">
             
            <div class="uk-grid">
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-purple">Pending Ride Requests</h1>
                </div>
            </div>
            
            <!-- switcher tabs -->
            <div class="uk-grid">
                <div class="uk-width-2-5@m ">
                    <div class="uk-grid">
                        <div class="uk-width-2-3@m" hidden>
                            <form action="admin-rides_searchresults.php" class="uk-search uk-search-default uk-align-left uk-flex-inline" method="POST">
                                <div class="">
                                    <span uk-search-icon></span>
                                    <input name="searchrides" class="uk-search-input uk-width-large uk-text-small " type="search" placeholder="Search Rides">
                                </div>
                                <input class="uk-button uk-button-primary" type="submit" value="Go">
                            </form>
                        </div>
                        <div class="uk-width-1-3@m" hidden>
                            <button class="uk-button uk-button-default " type="button">5 <span uk-icon="triangle-down"></span></button>
                            <div uk-dropdown="mode:click" class="uk-dropdown-results-selector">
                                <ul class="uk-nav uk-dropdown-nav">
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">25</a></li>
                                    <li><a href="#">50</a></li>
                                    <li><a href="#">100</a></li>
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <p>You have a total of <span class="uk-text-bold uk-text-purple"><?php echo $pending; ?></span> pending ride(s). </p>
            <hr>

            <!-- main content -->
            <div class="uk-container-padded">
                <div class="uk-overflow-auto">
                    <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                        <thead>
                                <tr>
                                <th class="uk-width-small" >Passenger</th>
                                <th class="uk-width-small">Pick-up Point</th>
                                <th class="uk-width-small">Drop-off Point</th>
                                <th class="uk-width-small">PU Date and Time</th>
                                <th class="uk-width-small">Date Added</th>
                                <th class="uk-width-small">Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if($result = mysqli_query($link, $sql)){
                                    if(mysqli_num_rows($result) > 0){

                                        while($row = mysqli_fetch_array($result)){
                                            $id = $row["id"];
                                            $passcode = $row["passcode"];
                                            $category = $row["category"];
                                            $name = $row["firstname"] ." ". $row['lastname'];
                                            $dropoff = $row["dropoff"];
                                            $pickup = $row["pickup"];
                                            $date_old = $row["date"];
                                            $date = date("M, d, Y", strtotime($date_old));
                                            $time = $row["time"];
                                            $status = $row["status"];
                                            $message = $row["message"];
                                            $budget = $row["budget"];
                                            $date_added_old = $row["created_at"];
                                            $date_added = date("M, d, Y", strtotime($date_added_old));


                                            echo '<tr> 
                                                    <td class="uk-text-wrap">'. '<span class="uk-text-bold uk-text-green">'. "<a class='uk-text-bold uk-text-green' href='pending-rideview.php?id=".$row['id']."' title='View Ride Details' data-toggle='tooltip'>$passcode</a>"  . '</span>'. '<br>'. $category.'<hr class="uk-hidden@s">' .'</td>
                                                    <td class="uk-text-wrap">'.'<p class="uk-hidden@s uk-text-bold">'.'Pick-up Point:'.'</p>'.$pickup.'</td>  
                                                    <td class="uk-text-wrap">'.'<p class="uk-hidden@s uk-text-bold">'.'Drop-off Point:'.'</p>'. $dropoff.'</td>
                                                    <td class="uk-text-wrap">'.'<p class="uk-hidden@s uk-text-bold uk-margin-remove-bottom">'.'Pick-up Date & Time:'.'</p>'.$date. '<br>' . $time .'</td>
                                                    <td class="uk-text-wrap">'. '<p class="uk-hidden@s uk-text-bold">'.'Requested Date:'.'</p>'. $date_added.'</td>  
                                                    <td class="uk-text-wrap">'.'<p class="uk-hidden@s uk-text-bold">'.'Additional Message:'.'</p>'.$message.'</td>  
                                                </tr>';
                                        }
                                        // Free result set
                                        mysqli_free_result($result);
                                    } else{
                                        echo "No records matching your query were found.";
                                    }
                                } else{
                                    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                                }
                            
                            // Close connection
                            mysqli_close($link);
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <ul class="uk-breadcrumb">
                <li>You are here:</li>
                <li><a href="#">Pending Rides</a></li>
            </ul>
        </div>
    </div>
    <!-- end main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>