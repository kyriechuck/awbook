<?php
    session_start(); //session start
   
    // Include config file
    include "config.php";

    $name = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
    $pickup = $_SESSION['pickup'];
    $dropoff = $_SESSION['dropoff'];
    $date = $_SESSION['date'];
    $time = $_SESSION['time'];
    $message = $_SESSION['message'];
    $budget = $_SESSION['budget'];

?>
<!doctype html>
<html>
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
       <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120133547-8"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-120133547-8');
        </script> 
        
        <title>Ride Details - AccessiWheels</title>
        <?php include "includes/header_meta.php" ?>
    </head>
<body>

<?php
    include ("includes/nav_driver.php");
?>

    <!-- start main section -->
    <div class="uk-section uk-section-default">
        <div class="uk-container uk-container-small">
            <h1 class="uk-text-bold uk-text-purple uk-margin-remove-bottom">Trips for <?php echo $_POST['query'] ;?></h1>
            <P class="uk-text uk-text-lead uk-margin-remove-top">Below are your upcoming scheduled trips.</P>

            <!-- main content -->
            <div class="uk-container-padded">
                
                <div class="uk-overflow-auto">
                    <table class='uk-table uk-table-responsive uk-table-middle uk-table-divider'>
                        <thead>
                            <tr>
                            <th class="uk-width-small" >Date and Time</th>
                            <th class="uk-width-small" >Passenger</th>
                            <th class="uk-width-small">Pick-up Point</th>
                            <th class="uk-width-small">Drop-off Point</th>
                            
                            <th class="uk-width-small" >Message</th>
                           <!--  <th class="uk-width-small">Date Added</th>
                            <th class="uk-table-shrink">Status</th> -->
                        </tr>
                    </thead>
                    <tbody>
            
                        <?php 
                            $query = $_POST['query']; 
                            // gets value sent over search form

                            // Attempt select query execution
                            $sql = "SELECT 
                                bookings.firstname as bookingFirstname, 
                                bookings.lastname as bookingLastname, 
                                bookings.category,
                                bookings.passcode,
                                bookings.pickup,
                                bookings.phone as bookingPhone,
                                bookings.dropoff,
                                bookings.date as bookingDate,
                                bookings.time,
                                bookings.message,
                                bookings.created_at as bookingCreatedat,
                                bookings.status as bookingStatus,
                                drivers.firstname as driverFirstname,
                                drivers.lastname as driverLastname
                                FROM drivers JOIN bookings on drivers.id = bookings.driver_id WHERE drivers.drivercode like '%".$query."%' AND bookings.status='scheduled' ORDER by date ASC LIMIT 5";

                            if($result = mysqli_query($link, $sql)){
                                if(mysqli_num_rows($result) > 0){
                                    
                                    while($row = mysqli_fetch_array($result)){
                                        $id = $row["id"];
                                        $passengercode = $row['passcode'];
                                        $category = $row["category"];
                                        $name = $row["bookingFirstname"] ." ". $row['bookingLastname'];
                                        $dropoff = $row["dropoff"];
                                        $pickup = $row["pickup"];
                                        $phone = $row["bookingPhone"];
                                        $date_old = $row["bookingDate"];
                                        $date = date("D-M d, Y", strtotime($date_old));
                                        $time = $row["time"];
                                        $status = $row["status"];
                                        $message = $row["message"];
                                        $budget = $row["budget"];
                                        $driver_id = $row["driver_id"];
                                        $driverFirstname = $row["driverFirstname"];
                                        $driverLastname = $row["driverLastname"];
                                        $date_added = $row["created_at"];

                                        echo '<tr> 
                                                <td class="uk-text-wrap">'.'<div class="uk-hidden@s uk-text-bold">Date and Tiime</div>'.$time. '<br>' . $date .'</td>
                                                <td class="uk-text-wrap">'.'<div class="uk-hidden@s uk-text-bold">Name</div>'. $name.'<br>'. $phone.'</td>
                                                <td class="uk-text-wrap">'.'<div class="uk-hidden@s uk-text-bold">Pick-up Point</div>' .$pickup.'</td>  
                                                <td class="uk-text-wrap">'.'<div class="uk-hidden@s uk-text-bold">Drop-off Point</div>'.$dropoff.'</td>
                                                <td class="uk-text-wrap">'.'<div class="uk-hidden@s uk-text-bold">Message</div>'.$message.'</td>
                                            </tr>';
                                    }
                                    
                                    echo "</table>";
                                    // Free result set
                                    mysqli_free_result($result);
                                } else{
                                    echo "No records matching your query were found.";
                                }
                            } else{
                                echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                            }
                            
                            // Close connection
                            mysqli_close($link);

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

           
            <!-- end: main content -->
            
        </div>
    </div>
    <!-- end: main section -->

    <?php include "includes/bottom_expand.php" ?>
    <?php include "includes/footer.php" ?>

</body>
</html>